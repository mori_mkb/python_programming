# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 11.7,8 Fraction calculator

# Learning Goals:
# I practise storing objects to a dict and using lambda function


class Fraction:
    """ This class represents one single fraction that consists of
        numerator and denominator """

    def __init__(self, numerator, denominator):
        """
        Constructor. Checks that the numerator and denominator are of
        correct type and initializes them.

        :param numerator: fraction's numerator
        :param denominator: fraction's denominator
        """

        if not isinstance(numerator, int) or not isinstance(denominator, int):
            raise TypeError
        elif denominator == 0:
            raise ValueError

        self.__numerator = numerator
        self.__denominator = denominator

    def return_string(self):
        """ Returns a string-presentation of the fraction in the format
        numerator/denominator """

        if self.__numerator * self.__denominator < 0:
            sign = "-"
        else:
            sign = ""
        return "{:s}{:d}/{:d}".format(sign, abs(self.__numerator),
                                     abs(self.__denominator))

    def simplify(self):
        """ Simplifies the fraction to an integer if its possible, otherwise
        it simplifies it to a fraction"""

        common_divisor_var = greatest_common_divisor(self.__numerator,
                                                     self.__denominator)
        self.__numerator = self.__numerator // common_divisor_var
        self.__denominator = self.__denominator // common_divisor_var

    def complement(self):
        if (self.__numerator or self.__denominator) < 0:
            return Fraction(abs(self.__numerator), abs(self.__denominator))
        else:
            return Fraction(-self.__numerator, self.__denominator)

    def reciprocal(self):
        return Fraction(self.__denominator, self.__numerator)

    def multiply(self, obj):
        return Fraction((self.__numerator * obj.__numerator),
                        (self.__denominator * obj.__denominator))

    def divide(self, obj):
        new_obj = obj.reciprocal()
        return Fraction((self.__numerator * new_obj.__numerator),
                        (self.__denominator * new_obj.__denominator))

    def add(self, obj):
        common_denominator = obj.__denominator * self.__denominator
        first_numerator = \
            (common_denominator / self.__denominator) * self.__numerator
        second_numerator = \
            (common_denominator / obj.__denominator) * obj.__numerator
        new_numerator = int(first_numerator + second_numerator)

        return Fraction(new_numerator, common_denominator)

    def deduct(self, obj):
        common_denominator = obj.__denominator * self.__denominator
        first_numerator = \
            (common_denominator / self.__denominator) * self.__numerator
        second_numerator = \
            (common_denominator / obj.__denominator) * obj.__numerator
        new_numerator = int(first_numerator - second_numerator)

        return Fraction(new_numerator, common_denominator)

    def __lt__(self, obj):
        return (self.__numerator/self.__denominator) < \
               (obj.__numerator/obj.__denominator)

    def __gt__(self, obj):
        return (self.__numerator/self.__denominator) > \
               (obj.__numerator/obj.__denominator)

    def __str__(self):
        return self.return_string()


def greatest_common_divisor(a, b):
    """
    Euclidean algorithm.
    """

    while b != 0:
        a, b = b, a % b
    return a


def main():

    fractions = {}
    while True:
        command = str(input("> "))
        if command in ["add", "quit", "print", "list", "file",
                       "*", "/", "+", "-"]:

            operators = {
                "*": lambda firstFrac, secFrac: firstFrac.multiply(secFrac),
                "/": lambda firstFrac, secFrac: firstFrac.divide(secFrac),
                "+": lambda firstFrac, secFrac: firstFrac.add(secFrac),
                "-": lambda firstFrac, secFrac: firstFrac.deduct(secFrac)
            }

            if command in operators:
                first_op = input("1st operand: ")
                if first_op in fractions:

                    second_op = input("2nd operand: ")
                    if second_op in fractions:
                        print(f"{fractions[first_op]} {command} "
                              f"{fractions[second_op]} = ", end="")
                        result_obj = operators[command](fractions[first_op],
                                                        fractions[second_op])
                        print(f"{result_obj}")
                        result_obj.simplify()
                        print(f"simplified {result_obj}")
                    else:
                        print(f"Name {second_op} was not found")

                else:
                    print(f"Name {first_op} was not found")

            elif command == "add":
                user_fraction = input("Enter a fraction in the "
                                      "form integer/integer: ")
                user_fraction_name = input("Enter a name: ")

                try:
                    first_val = int(user_fraction.split("/")[0])
                    second_val = int(user_fraction.split("/")[1])
                    fractions[user_fraction_name] = \
                        (Fraction(first_val, second_val))

                except ValueError:
                    continue

            elif command == "print":
                user_fraction_name = input("Enter a name: ")

                if user_fraction_name in fractions:
                    print(f"{user_fraction_name} = "
                          f"{fractions[user_fraction_name]}")
                else:
                    print(f"Name {user_fraction_name} was not found")

            elif command == "list":
                for fraction in sorted(fractions):
                    print(f"{fraction} = {fractions[fraction]}")

            elif command == "file":
                file_name = input("Enter the name of the file: ")

                try:
                    file = open(file_name, "r")
                    for line in file:
                        line = line.strip().split("=")
                        fractions[line[0]] = \
                            Fraction(int(line[1].split("/")[0]),
                                     int(line[1].split("/")[1]))

                except:
                    print("Error: the file cannot be read.")

            elif command == "quit":
                print("Bye bye!")
                break

        else:
            print("Unknown command!")


main()
