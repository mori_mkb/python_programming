# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 11.10 Getting to know the CSV library

# Learning Goals:
# Learning the use of the Python documentation and getting to know the
# CSV library.

import csv


def main():
    input_file_name = input('Enter the name of the input file: ')
    input_user_dialect = input('and its dialect: ')
    output_file_name = input('Enter the name of the output file: ')
    output_user_dialect = input('and its dialect: ')
    try:
        if input_user_dialect in csv.list_dialects() and output_user_dialect in csv.list_dialects():
            with open(input_file_name, newline="") as csvfile:
                reader = csv.reader(csvfile)

            with open(output_file_name, 'w', newline="") as csvfilewrite:
                writer = csv.writer(csvfilewrite, dialect=output_user_dialect)
                for row in reader:
                    writer.writerow(row)
                print()
                print(f"File {input_file_name} has been converted "
                      f"into {output_user_dialect}. ")
        else:
            print()
            print("The given dialect is wrong.")
            
    except (IOError, csv.Error):
        print()
        print("There was an error in handling the file.")


main()
