# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 11.9 Fraction calculator

# Learning Goals:
# Choose your learning goal: EITHER you practise combining data structures
# more OR additionally you practise designing and implementation
# of a class interface.


class Matrix:

    def __init__(self, name):
        self.__name = name
        self.__mat = []

    def fill_matrix(self, row):
        self.__mat.append(row)

    def scalar_product(self, operand):
        product = []
        for row in self.__mat:
            product.append([(operand * value) for value in row])
        return product

    def matrix_product(self, obj):

        product = [[sum(a * b for a, b in zip(A_row, B_col))
                    for B_col in zip(*obj.__mat)] for A_row in self.__mat]

        return product

    def productibale(self, obj):
        if len(self.__mat[0]) == len(obj.__mat):
            return True
        else:
            return False

    def show_matrix(self):
        return self.__mat


def main():

    matrix_list = {}
    while True:
        command = str(input("> "))
        if command in ["add", "quit", "print", "list", "scalarproduct",
                       "matrixproduct"]:

            if command == "add":
                try:
                    file_name = input("Enter the name of the matrix file: ")
                    matrix_name = input("Enter a name for the matrix: ")
                    file = open(file_name, "r")
                    matrix_list[matrix_name] = Matrix(matrix_name)
                    for line in file:
                        line = [int(x) for x in line.strip().split(" ")]
                        matrix_list[matrix_name].fill_matrix(line)

                    matrix_list[matrix_name].show_matrix()
                except:
                    continue

            elif command == "print":
                mat_name = str(input("Name: "))
                # if isinstance(mat_name, Matrix):
                #     mat_name.show_matrix()
                # else:
                #     print("no")
                if mat_name in matrix_list:
                    print(f"{mat_name} = {matrix_list[mat_name].show_matrix()}")
                else:
                    print(f"Name {mat_name} was not found")

            elif command == "list":
                for mat in matrix_list:
                    print(f"{mat} = {matrix_list[mat].show_matrix()}")

            elif command == "scalarproduct":
                mat_name = str(input("Enter the name: "))
                if mat_name in matrix_list:
                    operand = int(input("Enter a multiplier: "))
                    print(f"{operand} * {matrix_list[mat_name].show_matrix()}")
                    print(f"= {matrix_list[mat_name].scalar_product(operand)}")
                else:
                    print("The Matrix does not exist in my memory :( ")

            elif command == "matrixproduct":

                first_op = input("1st name: ")
                if first_op in matrix_list:

                    second_op = input("2nd name: ")
                    if second_op in matrix_list:

                        if matrix_list[first_op].productibale(matrix_list[second_op]):

                            print(f"{matrix_list[first_op].show_matrix()}")
                            print(f"* {matrix_list[second_op].show_matrix()}")

                            print(f"= {matrix_list[first_op].matrix_product(matrix_list[second_op])}")

                        else:
                            print("Error, the dimensions of the matrices don't match")

            elif command == "quit":
                print("Bye bye!")
                break




        else:
            print("Unknown command!")


main()