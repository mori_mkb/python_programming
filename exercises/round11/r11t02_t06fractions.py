# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 11.2,3,4,5,6 Fractions

# Learning Goals:
# To practice implementing a class and an object in Python.
# Learning how two objects of a similar type can communicate with each
# other in Python program code.
# Getting acquainted with the comparison methods (__lt__, __le__ jne.)
# presented in Python documentation.
# To familiarize yourself with the method __str___, which forms a presentation
# in the string format.


class Fraction:
    """ This class represents one single fraction that consists of
        numerator and denominator """

    def __init__(self, numerator, denominator):
        """
        Constructor. Checks that the numerator and denominator are of
        correct type and initializes them.

        :param numerator: fraction's numerator
        :param denominator: fraction's denominator
        """

        if not isinstance(numerator, int) or not isinstance(denominator, int):
            raise TypeError
        elif denominator == 0:
            raise ValueError

        self.__numerator = numerator
        self.__denominator = denominator

    def return_string(self):
        """ Returns a string-presentation of the fraction in the format
        numerator/denominator """

        if self.__numerator * self.__denominator < 0:
            sign = "-"
        else:
            sign = ""
        return "{:s}{:d}/{:d}".format(sign, abs(self.__numerator),
                                     abs(self.__denominator))

    def simplify(self):
        """ Simplifies the fraction to an integer if its possible, otherwise
        it simplifies it to a fraction"""

        common_divisor_var = greatest_common_divisor(self.__numerator,
                                                     self.__denominator)
        self.__numerator = self.__numerator // common_divisor_var
        self.__denominator = self.__denominator // common_divisor_var

    def complement(self):
        if (self.__numerator or self.__denominator) < 0:
            return Fraction(abs(self.__numerator), abs(self.__denominator))
        else:
            return Fraction(-self.__numerator, self.__denominator)

    def reciprocal(self):
        return Fraction(self.__denominator, self.__numerator)

    def multiply(self, obj):
        return Fraction((self.__numerator * obj.__numerator),
                        (self.__denominator * obj.__denominator))

    def divide(self, obj):
        new_obj = obj.reciprocal()
        return Fraction((self.__numerator * new_obj.__numerator),
                        (self.__denominator * new_obj.__denominator))

    def add(self, obj):
        common_denominator = obj.__denominator * self.__denominator
        first_numerator = \
            (common_denominator / self.__denominator) * self.__numerator
        second_numerator = \
            (common_denominator / obj.__denominator) * obj.__numerator
        new_numerator = int(first_numerator + second_numerator)

        return Fraction(new_numerator, common_denominator)

    def deduct(self, obj):
        common_denominator = obj.__denominator * self.__denominator
        first_numerator = \
            (common_denominator / self.__denominator) * self.__numerator
        second_numerator = \
            (common_denominator / obj.__denominator) * obj.__numerator
        new_numerator = int(first_numerator - second_numerator)

        return Fraction(new_numerator, common_denominator)

    def __lt__(self, obj):
        return (self.__numerator/self.__denominator) < \
               (obj.__numerator/obj.__denominator)

    def __gt__(self, obj):
        return (self.__numerator/self.__denominator) > \
               (obj.__numerator/obj.__denominator)

    def __str__(self):
        return self.return_string()


def greatest_common_divisor(a, b):
    """
    Euclidean algorithm.
    """

    while b != 0:
        a, b = b, a % b
    return a


def main():
    print("Enter fractions in the format integer/integer.")
    user_fraction = input("One fraction per line. "
                          "Stop by entering an empty line.\n")
    fractions = []

    while user_fraction != "":
        user_fraction = user_fraction.split("/")
        fractions.append(Fraction(int(user_fraction[0]),
                                  int(user_fraction[1])))
        user_fraction = input()

    print("The given fractions in their simplified form:")
    for fraction in fractions:

        print(f"{fraction} = ", end="")
        fraction.simplify()
        print(f"{fraction}")

    # frac = Fraction(37,39)
    # print(frac.return_string())
    # complement = frac.complement()
    # print(complement.return_string())
    # reciprocal = frac.reciprocal()
    # print(reciprocal.return_string())
    # frac1 = Fraction(2, 3)
    # frac2 = Fraction(3, 4)
    # product = frac1.multiply(frac2)
    # print(product.return_string())
    # product.simplify()
    # print(product.return_string())
    # frac1 = Fraction(4, 8)
    # frac2 = Fraction(2, 1)
    # quotient = frac1.divide(frac2)
    # print(quotient.return_string())
    # quotient.simplify()
    # print(quotient.return_string())

    # frac1 = Fraction(2, 3)
    # frac2 = Fraction(1, 6)
    # sum = frac1.add(frac2)
    # print(sum.return_string())
    # sum.simplify()
    # print(sum.return_string())
    #
    # frac1 = Fraction(2, 3)
    # frac2 = Fraction(1, 6)
    # difference = frac1.deduct(frac2)
    # print(difference.return_string())
    # difference.simplify()
    # print(difference.return_string())

    # frac1 = Fraction(1, 2)
    # frac2 = Fraction(2, 3)
    # print(frac1 < frac2)
    # print(frac1 > frac2)

    # frac = Fraction(8, 18)
    # print(frac)
    # frac.simplify()
    # print(frac)

main()
