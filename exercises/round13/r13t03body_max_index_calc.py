# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 13.3 Body mass index calculator

# Learning Goals:
# I learn to implement a simple graphical userinterface that contains text
# fields and a button.


from tkinter import *


class Userinterface:

    def __init__(self):
        self.__mainwindow = Tk()
        # TODO: Change the title of the main window to be "BMI calculator"
        self.__mainwindow.title("BMI calculator")

        # TODO: Add GUI components to make the GUI understandable for the
        # user, for example labels to indicate what the user should write
        # in the Entry-components.

        self.__weight_label = Label(self.__mainwindow, text="Enter Weight: ")
        self.__height_label = Label(self.__mainwindow, text="Enter Height: ")

        self.__weight_label.grid(row=0, column=0, sticky=N + E + S + W)
        self.__height_label.grid(row=1, column=0, sticky=N + E + S + W)


        # TODO: Create an Entry-component for the weight.
        self.__weight_value = Entry(self.__mainwindow, width=10)
        # TODO: Create an Entry-component for the height.
        self.__height_value = Entry(self.__mainwindow, width=10)

        # TODO: Create a Button that will call the calculate_BMI-method.
        # TODO: Change the colour of the Button to something else than
        # the default colour.
        self.__calculate_button = Button(self.__mainwindow, bg="blue",
                                         text="Calculate",
                                         command=self.calculate_BMI)
        # TODO: Create a Label that will show the decimal value of the BMI
        # after it has been calculated.
        self.__result_text = Label(self.__mainwindow, text="")
        # TODO: Create a Label that will show a verbal description of the BMI
        # after the BMI has been calculated.
        self.__explanation_text = Label(self.__mainwindow, text="")
        # TODO: Create a button that will call the stop-method.
        self.__stop_button = Button(self.__mainwindow,
                                    text="Quit",
                                    command=self.stop)

        # TODO: Place the components in the GUI as you wish.
        # If you read the Gaddis book, you can use pack here instead of grid!
        self.__weight_value.grid(row=0, column=1)
        self.__height_value.grid(row=1,column=1)
        self.__calculate_button.grid(row=2, column=0, sticky=N + E + S + W)
        self.__stop_button.grid(row=2, column=1, sticky=N + E + S + W)
        self.__result_text.grid(row=3, column=0, columnspan=2, sticky=E + W)
        self.__explanation_text.grid(row=4, column=0, columnspan=2, sticky=E + W)

    # TODO: Implement this method.
    def calculate_BMI(self):
        """ Section b) This method calculates the BMI of the user and
            displays it. First the method will get the values of
            height and weight from the GUI components
            self.__height_value and self.__weight_value.  Then the
            method will calculate the value of the BMI and show it in
            the element self.__result_text.

            Section e) Last, the method will display a verbal
            description of the BMI in the element
            self.__explanation_text.
        """

        try:
            height = float(self.__height_value.get())/100
            weight = float(self.__weight_value.get())
            if height > 0 and weight > 0:
                result = weight/pow(height, 2)
                self.__result_text.configure(text=format(result, "0.2f"))

                if 18.5 <= result <= 25:
                    self.__explanation_text.configure(text="Your weight is normal.")
                elif result > 25:
                    self.__explanation_text.configure(text="You are overweight.")
                elif result < 18.5:
                    self.__explanation_text.configure(text="You are underweight.")

            else:
                self.__explanation_text.configure(text="Error: height and weight "
                                                  "must be positive.")
                self.reset_fields()
        except:
            self.__explanation_text.configure(text="Error: height and weight "
                                               "must be numbers.")
            self.reset_fields()

    # TODO: Implement this method.
    def reset_fields(self):
        """ In error situations this method will zeroize the elements
            self.__result_text, self.__height_value, and self.__weight_value.
        """

        self.__result_text.configure(text="")
        self.__height_value.delete(0, END)
        self.__weight_value.delete(0, END)

    def stop(self):
        """ Ends the execution of the program.
        """
        self.__mainwindow.destroy()

    def start(self):
        """ Starts the mainloop.
        """
        self.__mainwindow.mainloop()


def main():
    ui = Userinterface()
    ui.start()


main()
