# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 13.5 Clicking counter

# Learning Goals:
# I implement a simple closure. (This is not a topic of this course, but it is
# very useful to understand if you want to learn JavaScript (that is very
# useful to know if you want to do webprogramming).)


def create_a_counter():
    number = [0]

    def counter():
        number[0] +=1
        return number[0]
    return counter


first_counter = create_a_counter()
second_counter = create_a_counter()
print(first_counter())
print(second_counter())
print(first_counter())
print(first_counter())
print(second_counter())