# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 10.2.3 Scoring for Mölkky

# Learning Goals:
# I will learn how to implement a class using Python.


# TODO: Implement the class Player here
class Player:

    def __init__(self, name):
        self.__name = name
        self.__points = 0
        self.__scores = []
        self.__rate = []

    def get_name(self):
        return self.__name

    def add_points(self, points):

        self.__points = self.__points + points
        self.__scores.append(points)

        if points > 0:
            self.__rate.append(1)
        else:
            self.__rate.append(0)

        if self.__points > 50:
            self.__points = 25
            print(f"{self.__name} gets penalty points!")

        elif points > sum(self.__scores) / len(self.__scores) \
                and len(self.__scores) > 1:
            print(f"Cheers {self.__name}!")

        elif 40 <= self.__points <= 49:
                print(f"{self.__name} needs only {50 - self.__points} points. "
                      f"It's better to avoid knocking down the pins "
                      f"with higher points.")

    def get_points(self):
        return self.__points

    def has_won(self):
        return self.__points == 50

    def calc_rate(self):
        if len(self.__rate) == 0:
            return 0.0
        else:
            return (sum(self.__rate) / len(self.__rate)) * 100


def main():

    # Here we define two variables which are the objects initiated from the
    # class Player. This is how the constructor of the class Player
    # (the method that is named __init__) is called!
    player1 = Player("Matti")
    player2 = Player("Teppo")

    throw = 1
    while True:
        if throw % 2 == 0:
            in_turn = player1
        else:
            in_turn = player2

        pts = int(input("Enter the score of player " + in_turn.get_name() +
                        " of throw " + str(throw) + ": "))
        in_turn.add_points(pts)

        if in_turn.has_won():
            print("Game over! The winner is " + in_turn.get_name() + "!")
            return

        print("")
        print("Scoreboard after throw " + str(throw) + ":")
        print(player1.get_name() + ":", player1.get_points(), "p,","hit percentage", format(player1.calc_rate(),"0.1f"))
        print(player2.get_name() + ":", player2.get_points(), "p,","hit percentage",format(player2.calc_rate(),"0.1f"))
        print("")

        throw += 1

main()

