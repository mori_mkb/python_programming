# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 10.3 Transforming the time format of measurement results

# Learning Goals:
# I practise processing of larger amounts of data in a Python program.


def main():

    try:
        read_file_name = str(input('Enter the name of the file to be read: '))
        write_file_name = str(
            input('Enter the name of the file to be written: '))
        opened_file = open(read_file_name, 'r')

        row1 = opened_file.readline().rstrip()
        write_file = open(write_file_name, "w")
        write_file.write(row1+"\n")
        first_list = []
        second_list = []
        for line in opened_file:
            if not line.startswith("Time"):
                line = line.split(";",1)
                second_list = line[0].split(":")
                hour_to_sec = int(second_list[0])*3600
                min_to_se = int(second_list[1])*60
                total = hour_to_sec + min_to_se + int(second_list[2])
                print(total)
                write_file.write(str(total) + ";" + line[1])
        print("Information saved successfully.")
    except IOError:
        print("Error in reading the file!")

main()