# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 8.4.2 Numbering the file rows doesn't succeed

# Learning Goals:
# I learn the details related to processing files.


def main():

    try:
        # enter the name of the file
        file_name = str(input('Enter the name of the file: '))

        # open file in read mode
        open_file = open(file_name, 'r')

        # finding the number of lines
        line_counter = 0
        for line in open_file:
            line_counter += 1

        # finding the length of the line_counter variable for spacing format
        length_row_num = str(len(str(line_counter)))

        # create the spacing format string for print function
        string = '{0:' + length_row_num + 'd}' + ' ' + '{1}'

        # resetting the file handler to first position
        open_file.seek(0)

        # printing lines from first position with their row
        # number and proper space
        row_num = 0
        for line in open_file:
            row_num += 1
            line = line.rstrip()
            print(string.format(row_num, line))

        # closing the file
        open_file.close()

    except IOError:
        print('There was an error in reading the file.')


main()


