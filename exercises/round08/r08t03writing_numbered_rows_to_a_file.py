# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 8.3.1 Writing numbered rows to a file

# Learning Goals:
# I learn the details related to processing files.


def main():

    try:
        # enter the name of the file
        file_name = str(input('Enter the name of the file: '))

        # open file in read mode
        open_file = open(file_name, 'w')

        # getting the message from user
        messages = read_message()

        # checks whether a message entered by user
        if len(messages) > 0:
            # create the position of variables and proper spacing
            string = '{0:' + str(len(str(len(messages)))) + 'd}' + ' ' + '{1}'
            row_num = 0

            # add messages with proper spacing to the file
            for message in messages:
                row_num += 1
                message = message.strip()
                row_string = string.format(row_num, message)
                open_file.write(row_string + '\n')

            open_file.close()
            print('File {} has been written.'.format(file_name))

        else:
            print('No message has been added to the file')

    except:
        print("The file does not open")


# reading the message from user line by line and adding to the list
def read_message():

    message = str(input('Enter rows of text. Quit by entering an '
                        'empty row.\n'))
    message_list = list()
    while message != "":
        message_list.append(message)
        message = input()

    return message_list


main()
