# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 8.7 OS library

# Learning Goals:
# Getting acquainted with acquisition of file list and
# renaming of files (topic external to course materials).

# importing needed libraries
import os


def fix_filenames(folder_name):
    """ Rename the mp3 files in given folder name
    :param folder_name: str, folder path
    """

    # defining the delimiters
    format_delimiter = '.'
    dash_delimiter = '-'

    # going through all files in the given folder name
    for song in os.listdir(folder_name):

        # only mp3 files are accepted
        if song.endswith('.mp3'):

            try:
                # splitting the song with '-' delimiter
                split_song = song.split('-')

                # checking the file has structured with 3 parts
                if len(split_song) == 3:

                    # file starts with integer is correct structure
                    int(split_song[0])

                    # splitting the second part of the file with '.' delimiter
                    mp3_format = split_song[2].split('.')

                    # joining the parts to make the correct structure
                    new_song_name = dash_delimiter.join([mp3_format[0],
                                                         split_song[1]])
                    # joining all parts to make correct format "song.mp3"
                    new_song_format = format_delimiter.join([new_song_name,
                                                            mp3_format[1]])
                    # rename the file with new built name
                    os.rename(os.path.join(folder_name, song),
                              os.path.join(folder_name, new_song_format))

            # any error happens means the file is not acceptable for change
            except:
                continue


#path = 'C:\\Users\Morteza\Desktop\gg'
#fix_filenames(path)
