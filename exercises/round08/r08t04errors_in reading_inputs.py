# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 8.4.1 Errors in reading the inputs

# Learning Goals:
# Learning to implement a try-except-structure for processing errors.


def main():
    width = read_input("Enter the width of a frame: ")
    height = read_input("Enter the height of a frame: ")
    mark = input("Enter a print mark: ")

    print_box(width, height, mark)


# Read the input until it gets a positive and none zero value
def read_input(question):
    flag = True
    while flag:
        try:
            user_var = int(input(question))
            if user_var > 0:
                flag = False
        except ValueError:
            flag = True

    return user_var


# the function prints a filled box based on value that are passed
def print_box(width_var, height_var, mark_var):

    for i in range(int(height_var)):
        print()
        for j in range(int(width_var)):
            print(mark_var, end="")


main()
