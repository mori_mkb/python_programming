# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 8.6 Errors of calculating scores

# Learning Goals:
# I learn to handle the error situations when processing
# information that is stored in a file.


def main():

    try:
        # getting the file name from user
        file_name = str(input('Enter the name of the score file: '))

        # opening the file
        open_file = open(file_name, 'r')

        # calling the function that gets the score from file
        get_scores(open_file)

        # closing the file
        open_file.close()

    # error detection
    except IOError or FileNotFoundError:
        print('There was an error in reading the file.')


# gets the scores from file and add them the dictionary
def get_scores(file):

    try:
        # creating the scores dictionary
        scores = dict()

        # adding the contestants and scores to the dictionary
        for line in file:
            line = line.strip().split(' ')
            scores[line[0]] = scores.get(line[0], 0) + int(line[1])

        # printing the sorted contestants in alphabetical
        # order with their scores
        print_scores(scores)

    # error detection
    except IndexError:
        print('There was an erroneous line in the file:')
        print(line[0])

    except ValueError:
        print('There was an erroneous score in the file:')
        print(line[1])


# printing the sorted contestants in alphabetical order with their scores
def print_scores(scores):

    print('Contestant score:')

    # printing the scores one by one in alphabetical order
    for key in sorted(scores.keys()):
        print('{0} {1}'.format(key, scores[key]))


main()
