bored = str(input('Bored? (y/n) ')).lower()
while True:
    if bored == 'y' or bored == 'Y':
        print('Well, let\'s stop this, then.')
        break
    elif bored == 'n' or bored == 'N':
        bored = str(input('Bored? (y/n) ')).lower()
        continue
    else:
        print('Incorrect entry.')
        bored = str(input('Please retry: '))
        continue