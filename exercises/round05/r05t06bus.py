# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 5.6 Bus
# Practicing the implementation of a slightly more complicated list program
# and once again reviewing the operations of the remainder operator.


def main():

    bus_schedule = [630, 1015, 1415, 1620, 1720, 2000]

    user_input = int(input('Enter the time (as an integer): '))

    next_bus_index = 0
    for index in range(len(bus_schedule)):
        if bus_schedule[index] >= user_input:
            next_bus_index = index
            break

    counter = 3
    total_time = 0
    again = True
    print('The next buses leave:')
    while again:
        for time in bus_schedule[next_bus_index:]:
            print(time)
            counter -= 1
            total_time += time
            if total_time % 3720 == 0:
                break
            if counter == 0:
                again = False
                break
        next_bus_index = 0


main()
