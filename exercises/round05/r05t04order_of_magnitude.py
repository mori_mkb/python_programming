# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 5.4.3 A function for reviewing a list's order of magnitude
# members have an equal value
# Familiarizing yourself with the prebuilt operations the list contains


def main():

    if is_the_list_in_order([37, 42, 43]):
        print("True")
    else:
        print("False")


# Checks weather a list is in order or not
def is_the_list_in_order(list_value):

    if sorted(list_value) == list_value:
        return True
    else:
        return False


main()
