# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  5.2 Number series
# The purpose of the first task is reviewing how
# the for and range functions operate


MAX_RANGE = 100
MIN_RANGE = 0
STEP = 2


def main():

    for i in list(range(MIN_RANGE,MAX_RANGE+1, STEP)):
        print(i)

    for j in list(range(MAX_RANGE, MIN_RANGE-1, -STEP)):
        print(j)


main()
