# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  5.4.1 A list as a return value
# To familiarize yourself with the prebuilt operations of a list


def main():

    read_value = int(input('How many numbers do you want to process: '))
    # Call function to ask numbers and create the list
    numbers = input_to_list(read_value)

    # Find how many times a number entered
    item = int(input('Enter the number to be searched: '))
    if numbers.count(item) > 0:
        string = str(item) + " shows up " + str(numbers.count(item)) + " times"
    else:
        string = str(item) + " is not"

    print(string, 'among the numbers you have entered.')


# Create the list and Add numbers
def input_to_list(read_size):

    number_list = []

    print('Enter', read_size, 'numbers:')
    for index in range(read_size):
        num = int(input())
        number_list.append(num)

    return number_list


main()
