# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  5.3.1 Going through the elements of a list
# To learn about indexing a list using the for command,
# going through the elements directly.

LOOP_SIZE = 5


def main():

    # Create the list
    numbers = []
    print('Give', LOOP_SIZE, 'numbers:')

    # Add only positive number to the list
    for index in range(LOOP_SIZE):
        print('Next number: ', end='')
        num = int(input())
        if is_positive(num):
            numbers.append(num)

    # Print the list of positive numbers
    print('The numbers you entered that were greater than zero were:')
    for number in numbers:
        print(number)


# Is a value positive?
def is_positive(value):

    if value > 0:
        status = True
    else:
        status = False
    return status


main()
