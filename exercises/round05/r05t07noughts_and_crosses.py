# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 5.7.1 Noughts-and-crosses, barebones version
# Learning to use nested lists.


LIST_RANGE = 3
DELIMITER = ''
STOP_GAME = 9


def main():

    grid_list = [['.', '.', '.'],
                 ['.', '.', '.'],
                 ['.', '.', '.']]

    for index in grid_list:
        print(DELIMITER.join(index))

    # for row in range(ROW):
    #     for col in range(COL):
    #         print(grid_list[row][col], end='')
    #     print()

    turns = 0 # How many turns have been played

    # The game continues until the board is full.
    # 9 marks have been placed on the board when the player has been
    # switched 8 times.
    while turns < 9:

        # Change the mark for the player
        if turns % 2 == 0:
            mark = "X"
        else:
            mark = "O"
        coordinates = input("Player " + mark + ", give coordinates: ")

        try:
            x, y = coordinates.split(" ")
            x = int(x)
            y = int(y)

            turns = play_game(x, y, grid_list, mark, turns)

        except ValueError:
            print("Error: enter two integers, separated with spaces.")

        except IndexError:
            print("Error: coordinates must be between 0 and 2.")


# This function checks the user entered position to put the "O" or "X" based
# on the turn value. return the next turn value if the position is empty
def play_game(x_value, y_value, value_list, player_mark, turn_value):

    if value_list[y_value][x_value] == '.':
        value_list[y_value][x_value] = player_mark
        turn_value = turn_value + 1
        # printing the list
        for index in value_list:
            print(DELIMITER.join(index))
        # Only after 5 turns, game can have a winner
        if turn_value >= 5:
            turn_value = evaluate_winner(value_list, turn_value, player_mark)
        return turn_value
    else:
        print("Error: a mark has already been placed on this square.")
        return turn_value


# Evaluating the winner by checking the values
def evaluate_winner(value_list, turn_value, player_mark):

    for i in range(LIST_RANGE):
        # Checking the equality of row values
        if value_list[i][0] == value_list[i][1] == value_list[i][2] \
                and '.' not in value_list[i]:
            print_winner(player_mark)
            return STOP_GAME
        # Checking the equality of column values
        if value_list[0][i] == value_list[1][i] == value_list[2][i]:
            print_winner(player_mark)
            return STOP_GAME
    # Checking the equality of slanted starting from left
    if value_list[0][0] == value_list[1][1] == value_list[2][2]:
        print_winner(player_mark)
        return STOP_GAME
    # Checking the equality of slanted starting from right
    if value_list[0][2] == value_list[1][1] == value_list[2][0]:
        print_winner(player_mark)
        return STOP_GAME
    # If its last round of the game and no condition satisfied
    if turn_value == STOP_GAME:
        print('Draw!')

    return turn_value


# Print winner of the game
def print_winner(player_mark):
    print('The game ended, the winner is {}'.format(player_mark))


main()

