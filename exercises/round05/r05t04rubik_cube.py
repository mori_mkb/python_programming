# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 5.4.4 Rubik's Cube solving contests
# members have an equal value
# Getting more practice in the use of the list.
# Getting acquainted with some new list operations.

# The total time the user plays rubik game
TOTAL_ROUND = 5


def main():
    # Asking each round performance in seconds
    performances = get_performance()
    # Calculate the average after removing max and min performance
    average_per = get_average(performances)

    print('The official competition score is {:0.2f} seconds.'
          . format(average_per))


# Create a List and assign values entered by user
def get_performance():

    performance_list = []

    for each_round in range(TOTAL_ROUND):
        print('Enter the time for performance {}: '
              . format(each_round+1), end='')
        per = float(input())
        performance_list.append(per)

    return performance_list


# Calculate average after removing max and min performance from total
def get_average(value_list):

    min_per = min(value_list)
    max_per = max(value_list)
    total = 0.0

    for num in value_list:
        total += num

    total -= min_per
    total -= max_per
    average = total/(len(value_list)-2)

    return average


main()
