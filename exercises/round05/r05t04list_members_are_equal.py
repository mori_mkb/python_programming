# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  5.4.2 A function for comparing whether list
# members have an equal value
# Familiarizing yourself with the prebuilt operations the list contains

FIRST_INDEX = 0
EMPTY_SET = 0


def main():
    if are_all_members_same([0, 0, 1]):
        print('True')
    else:
        print('False')


# Search through the list to see all members are same
def are_all_members_same(list_value):
    length_list = len(list_value)
    if length_list == EMPTY_SET:
        return True
    elif length_list == list_value.count(list_value[FIRST_INDEX]):
        return True
    else:
        return False


main()
