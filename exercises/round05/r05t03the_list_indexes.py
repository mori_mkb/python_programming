# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  5.3.2 Going through the list indexes
# To practice the use of indexes in going through a list

LOOP_SIZE = 5


def main():

    # Create the list
    numbers = []
    print('Give', LOOP_SIZE, 'numbers:')

    # Add 5 number to the list
    for index in range(LOOP_SIZE):
        print('Next number: ', end='')
        num = int(input())
        numbers.append(num)

    # Print the list in reverse order
    print('The numbers you entered, in reverse order: ')
    for index in range(len(numbers)-1, -1, -1):
        print(numbers[index])


main()
