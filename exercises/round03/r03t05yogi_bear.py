# TIE-02106 Introduction to Programming
# Stuart Student, morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.5.2 The song "Yogi Bear"
# Learning that the function can use other functions as an aid.


def main():

    verse("I know someone you don't know", "Yogi")
    verse("Yogi has a best friend too", "Boo Boo")
    verse("Yogi has a sweet girlfriend", "Cindy")


# prints names based on repetition number
def repeat_name(name,rep_num):

    for i in range(rep_num):
        if rep_num > 1:
            print("{0}, {0} Bear".format(name))


# prints a verse and pass repetitions to repeat_name()
def verse(chorus, bear_name):

    print(chorus)
    print("{0}, {0}".format(bear_name))
    print(chorus)
    repeat_name(bear_name, 3)
    print(chorus)
    repeat_name(bear_name, 1)
    print("{0}, {0} Bear\n".format(bear_name))


main()
