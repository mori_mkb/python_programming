# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.6.2 Printing a box and checking the inputs
# practising functions in further detail.


def main():
    width = read_input("Enter the width of a frame: ")
    height = read_input("Enter the height of a frame: ")
    mark = input("Enter a print mark: ")

    print_box(width, height, mark)


# Read the input until it gets a positive and none zero value
def read_input(question):

    while True:
        user_var = int(input(question))
        if user_var > 0:
            return user_var


# the function prints a filled box based on value that are passed
def print_box(width_var, height_var, mark_var):

    for i in range(int(height_var)):
        print()
        for j in range(int(width_var)):
            print(mark_var, end="")


main()
