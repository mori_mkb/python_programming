# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.6.3 Parasetamol needed!
# Practising functions in more detail.


MAX_DAILY_DOSE = 4000
DOSE_PER_KG = 15
HOURS_CYCLE = 6


def main():

    weight = int(input("Patient's weight (kg): "))
    time = int(input("How much time has passed from"
                     " the previous dose (full hours): "))
    total_doze_24 = int(input("The total dose for the last 24 hours (mg): "))

    dose = calculate_dose(weight, time, total_doze_24)

    print("The amount of Parasetamol to give to the patient: ", dose)

    # calculate_dose assumes parameters to be of type int
    # and they should be passed in the order: weight, time, total_doze_24
    # (or more like the automated tests assume this)


# Calculate the correct dose that one person can take
def calculate_dose(weight_var, time_var, total_doze_24):

    max_six_usage = weight_var * DOSE_PER_KG

    if time_var < HOURS_CYCLE:
        return 0

    elif time_var >= HOURS_CYCLE:
        if total_doze_24 <= max_six_usage:
            return max_six_usage
        elif total_doze_24 > max_six_usage:
            dif = MAX_DAILY_DOSE - total_doze_24
            if dif >= max_six_usage:
                return max_six_usage
            else:
                return dif


main()
