# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.6.1 Triangle’s area
# Understanding what happens when a return value is passed
# from a function to the main function.

from math import sqrt


def main():

    first_side = input("Enter the length of the first side: ")
    second_side = input("Enter the length of the second side: ")
    third_side = input("Enter the length of the third side: ")

    estimated_area = area(float(first_side), float(second_side),
                          float(third_side))

    print("The triangle's area is", format(estimated_area, '0.1f'))


# The function estimate the area of triangle and returns the area
def area(first, second, third):

    semiperimeter = (first+second+third)/2
    estimate_area = sqrt(semiperimeter*(semiperimeter - first)*
                         (semiperimeter - second)*(semiperimeter - third))

    return estimate_area


main()
