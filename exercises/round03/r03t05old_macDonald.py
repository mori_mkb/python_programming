# TIE-02106 Introduction to Programming
# Stuart Student, morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.5.1 The song "Old MacDonald had a farm"
# Learning to create a function that uses parameters.
# Understanding the uses of the functions.


# This function sends a dictionary to print_verse function
def main():

    animal_sound_dict = {"cow":"moo",
                         "pig":"oink",
                         "duck":"quack",
                         "horse":"neigh",
                         "lamb":"baa"}

    for animal, sound in animal_sound_dict.items():
        print_verse(animal, sound)


# This function prints verses
def print_verse(animal_var, sound_var):

    chorus_body = "Old MACDONALD had a farm\n" \
                  "E-I-E-I-O\n" \
                  "And on his farm he had a {0}\n" \
                  "E-I-E-I-O\n" \
                  "With a {1} {1} here\n" \
                  "And a {1} {1} there\n" \
                  "Here a {1}, there a {1}\n" \
                  "Everywhere a {1} {1}\n" \
                  "Old MacDonald had a farm\n" \
                  "E-I-E-I-O\n"

    chorus_footer = "Old MACDONALD had a farm\n" \
                    "E-I-E-I-O\n" \
                    "And on his farm he had a {0}\n" \
                    "E-I-E-I-O\n" \
                    "With a {1} {1} here\n" \
                    "And a {1} {1} there\n" \
                    "Here a {1}, there a {1}\n" \
                    "Everywhere a {1} {1}\n" \
                    "Old MacDonald had a farm\n" \
                    "E-I-E-I-O"

    if animal_var != "lamb":
        print(chorus_body.format(animal_var, sound_var))
    else:
        print(chorus_footer.format(animal_var, sound_var))


main()

# a better solution was to send another argument with
# function to indicate the location of last item in
# dictionary but here we could only pass 2 args with function
