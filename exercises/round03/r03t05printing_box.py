# TIE-02106 Introduction to Programming
# Stuart Student, morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.5.3 Printing a box
# Understanding what happens when a parameter is
# passed from the main function to another function.


def main():
    width = input("Enter the width of a frame: ")
    height = input("Enter the height of a frame: ")
    mark = input("Enter a print mark: ")

    print_box(width, height, mark)


# the function prints a filled box based on value that are passed
def print_box(width_var, height_var, mark_var):

    for i in range(int(height_var)):
        print()
        for j in range(int(width_var)):
            print(mark_var,end="")



main()

