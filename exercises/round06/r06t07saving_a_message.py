# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.7 Saving a message

# Learning Goals:
# Learning to save strings in a list.


def main():
    print("Enter text rows to the message. Quit by entering an empty row.")
    msg = read_message()

    print("The same, shouting:")
    # Print the CAPITAL message line by line
    for line in msg:
        line = line.upper()
        print(line)


# Lets get the message line by line
def read_message():

    message = input()
    message_list = list()
    while message != "":
        message_list.append(message)
        message = input()

    return message_list


main()