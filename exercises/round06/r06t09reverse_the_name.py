# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.9 Reverse the names to be correct

# Learning Goals:
# To get acquainted with the prebuilt string operations in Python.


def reverse_name(name):
    """ Removes any extra spaces and comma between two words and reverse them
    :param name: str, two name separated by comma
    :return: name, reversed names without comma and added space between them
    """
    try:
        comma_index = name.index(',')  # cause value error if not found
        last_name = name[:comma_index]
        last_name = last_name.strip()
        first_name = name[comma_index+1:]  # goodbye comma
        first_name = first_name.strip()
        if first_name == "":
            name = last_name
        elif last_name == "":
            name = first_name
        else:
            name = first_name + " " + last_name

        return name

    except ValueError:
        return name


# =======ALTERNATIVE WAY========
# def reverse_name(name):
#     """ Removes any extra spaces and comma between two words and reverse them
#     :param name: str, two name separated by comma
#     :return: name, reversed names without comma and added space between them
#     """
#     correct_name = list()
#     words = name.split(',')
#     for word in range(len(words)-1, -1, -1):  # for i in reversed(words)
#         correct_name.append(words[word].strip())
#
#     name = ""
#     correct_name.insert(1, " ")
#     for word in correct_name:
#         name += word
#
#     return name
#
#
#print(reverse_name(","))
