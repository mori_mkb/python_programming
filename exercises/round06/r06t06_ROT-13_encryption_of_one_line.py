# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.6 ROT-13 encryption of one line

# Learning Goals:
# Getting acquainted with string structure, ie. how to handle the
# characters in a string using a for command and the [] operator.


# performs the ROT13 transformation for one character.
def encrypt(alphabet):
    regular_chars = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                     "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                     "w", "x", "y", "z"]

    encrypted_chars = ["n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
                       "y", "z", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                       "j", "k", "l", "m"]

    # only alphabetic characters are accepted
    if alphabet.isalpha():
        flag_upper = False
        if alphabet.isupper():
            flag_upper = True
            alphabet = alphabet.lower()
        index = regular_chars.index(alphabet)
        alphabet = encrypted_chars[index]

        if flag_upper:
            alphabet = alphabet.upper()

    return alphabet


#print(encrypt('3'))


# performs a ROT13 transformation for an entire string
def row_encryption(text):
    new_text = ""
    for letter in text:
        new_text += encrypt(letter)
    return new_text


#print(row_encryption("Happy, happy, joy, joy!"))