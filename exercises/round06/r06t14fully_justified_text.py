# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.14 Fully justified text

# Learning Goals:
# I learn to use a datastructure that contains multiple strings.


def main():

    word_list = get_words()
    #words = get_words_list(text)
    print_justified_text(word_list)


def get_words():
    """
    :return:
    """

    full_text = ""
    words = list()
    print("Enter text rows. Quit by entering an empty row.")
    row_text = str(input())
    while row_text != "":
        line = row_text.split(" ")
        for word in line:
            words.append(word)
        row_text = str(input())

    return words


# def get_words_list(text_value):
#     """
#     :param text_value:
#     :return:
#     """
#     print(text_value)
#     words = list()
#     for line in text_value:
#         line = line.split(" ")
#         print(line)
#         for word in line:
#             words.append(word)
#     print(words)
#     return words


def print_justified_text(words_list):
    """
    :param words_list:
    :return:
    """

    length_line = int(input("Enter the number of characters per line: "))
    print(words_list)
    char_counter = 0
    while char_counter < length_line:

        char_counter += 1


main()
