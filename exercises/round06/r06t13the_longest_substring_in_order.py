# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.13 The longest substring in order

# Learning Goals:
# Learning to design a somewhat more challenging
# algorithm for processing strings.


def longest_substring_in_order(text):
    """ Find the longest substring in alphabetic order
    :param text: str
    :return: str, longest substring in order
    """

    long_string = ""
    string = list()
    longest_string = ""

    for index in range(len(text)):
        # Is last letter of previous long string is greater than text letter
        if text[index] < long_string[-1:]:
            long_string = text[index]
            continue
        long_string += text[index]
        string.append(long_string)

    longest_string = max(string, key=len)
    return longest_string


print(longest_substring_in_order("acdkbarstyefgioprtyrtyx"))