# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.5 Vowels and consonants

# Learning Goals:
# Learning to go through the characters of
# a string using a repetition structure.


def main():

    vowels = "aeiou"
    counter_vowels = 0
    word = str(input("Enter a word: "))

    for letter in word:
        if letter in vowels:
            counter_vowels += 1

    count_consonants = len(word) - counter_vowels

    print("The word {0} contains {1} vowels and {2} "
          "consonants".format(word, counter_vowels, count_consonants))


main()
