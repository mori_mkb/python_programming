# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.11 Capitalization

# Learning Goals:
# Getting acquainted with prebuilt string operations in Python.
# Learning to use Python's documentation.


def capitalize_initial_letters(text):
    return text.lower().title()


#print(capitalize_initial_letters("drIVING cAR"))