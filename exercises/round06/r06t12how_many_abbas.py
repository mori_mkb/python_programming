# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.12 How many abbas?

# Learning Goals:
# Learning to design an algorithm related to processing the strings.


def count_abbas(text):
    """ Finds number of 'abba' in the text
    :param text: str
    :return: int, number of abbas
    """
    looking_for = "abba"
    counter = 0
    next = 4
    for index in range(len(text)):
        st = text[index:next]
        next += 1
        if st == looking_for:
            counter += 1

    return counter


#print(count_abbas('barbapapa'))