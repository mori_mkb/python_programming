# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.10 Forming an acronymt

# Learning Goals:
# Familiarizing yourself with the prebuilt string operations in Python.


def create_an_acronym(name):
    """ Requests a name as a parameter and returns its acronym
    :param name: str,
    :return: str,
    """

    name = name.title()
    acronym = ""
    for upperchar in name:
        if upperchar.isupper():
            acronym += upperchar

    return acronym


#print(create_an_acronym("central intelligence agency"))