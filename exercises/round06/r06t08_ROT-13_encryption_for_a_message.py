# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.8 ROT-13 encryption for a whole message

# Learning Goals:
# Learn to implement a new program by combining
# previously implemented functions

# This program encrypts a message and return the encrypted message.


def main():
    print("Enter text rows to the message. Quit by entering an empty row.")
    msg = read_message()

    # Lets encrypt the message line by line(I should not tell you this!!)
    encrypted_msg = list()
    for line in msg:
        line = row_encryption(line)
        encrypted_msg.append(line)

    print("ROT13:")
    # Print the encrypted message line by line
    for line in encrypted_msg:
        print(line)


def read_message():
    """ Lets get the message line by line
    :return: list, the whole message
    """
    message = input()
    message_list = list()
    while message != "":
        message_list.append(message)
        message = input()

    return message_list


def encrypt(alphabet):
    """ Performs the ROT13 transformation for one character.
    :param alphabet: str, one character
    :return: str, encrypted character
    """
    regular_chars = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                     "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                     "w", "x", "y", "z"]

    encrypted_chars = ["n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
                       "y", "z", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                       "j", "k", "l", "m"]

    # only alphabetic characters are accepted
    if alphabet.isalpha():
        flag_upper = False
        if alphabet.isupper():
            flag_upper = True
            alphabet = alphabet.lower()
        index = regular_chars.index(alphabet)
        alphabet = encrypted_chars[index]

        if flag_upper:
            alphabet = alphabet.upper()

    return alphabet


def row_encryption(text):
    """ Performs a ROT13 transformation for an entire string
    :param text: str, one line of string
    :return: str, encrypted line of string
    """
    new_text = ""
    for letter in text:
        new_text += encrypt(letter)
    return new_text


main()


