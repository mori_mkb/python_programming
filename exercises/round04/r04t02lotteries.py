# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 4.2.3 Lotteries
# Teaching yourself to find parts of the program that can be
# turned into functions. Rehearsing the creation of functions.
# When reading the task instructions, think about which operations
# should be implemented as functions. You should come up with two functions.


def main():

    total_balls = int(input("Enter the total number of lottery balls: "))
    drawn_balls = int(input("Enter the number of the drawn balls: "))

    if is_positive(total_balls, drawn_balls):
        if drawn_is_not_greater(total_balls, drawn_balls):
            wining_chance = calculate_prob(total_balls, drawn_balls)
            print("The probability of guessing all {0} balls "
                  "correctly is 1/{1:.0f}".format(drawn_balls, wining_chance))


# This function calculate the probability of wining
def calculate_prob(t_balls, d_balls):

    nominator = fact_calc(t_balls)
    denominator = fact_calc((t_balls - d_balls)) * fact_calc(d_balls)
    return nominator/denominator


# Calculates the factorial
def fact_calc(value):

    if value == 0 or value == 1:
        return 1
    else:
        num = value
        while num != 1:
            num = num-1
            fact = value * num
            value = fact
        return value


# Are input values entered by user positive,
def is_positive(value_one, value_two):

    if value_one > 0 and value_two > 0:
        status = True
    else:
        print("The number of balls must be a positive number.")
        status = False

    return status


# Return false if drawn balls is greater than or equal to total balls
def drawn_is_not_greater(value_one, value_two):

    if value_two >= value_one:
        print("At most the total number of balls can be drawn.")
        status = False
    else:
        status = True

    return status


main()
