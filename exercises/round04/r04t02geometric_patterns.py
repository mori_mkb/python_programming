# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  4.2.4 Geometric patterns
# Learning to find program parts that can be turned to functions.
# Rehearsing the creation of functions. Design your program
# using as many functions as possible in its implementation.

import math


# List of shapes to be selected by user
def menu():

    while True:
        answer = input(
            "Enter the pattern's first letter, q stops this (s/r/q): ")
        if answer == "s":
            square()

        elif answer == "r":
            rectangle()

        elif answer == "c":
            circle()

        elif answer == "q":
            return

        else:
            print("Incorrect entry, try again!")

        print()  # Empty row for the sake of readability


def main():
    menu()

    print("Goodbye!")


# Asks user to enter a correct value
def value_curious(shape_name, side=None):

    value = 0
    while True:
        if shape_name == 'circle':
            value = float(input("Enter the {}'s radius: "
                                .format(shape_name)))

        elif shape_name == 'square':
            value = float(input("Enter the length of the {0}'s side: "
                                .format(shape_name)))

        elif shape_name == 'rectangle':
            value = float(input("Enter the length of the {0}'s side {1:d}: "
                                .format(shape_name, side)))

        if is_positive(value):
            return value


# Assign Circle's parameters and pass them to print
def circle():

    shape = "circle"
    radius = value_curious(shape)

    rec_circumference = calculate_circumference(shape, radius)
    rec_area = calculate_area(shape, radius)

    print_values(rec_circumference, rec_area)


# Assign Square's parameters and pass them to print
def square():

    shape = "square"
    square_side = value_curious(shape)

    square_circumference = calculate_circumference(shape, square_side)
    square_area = calculate_area(shape, square_side)

    print_values(square_circumference, square_area)


# Assign Rectangle's parameters and pass them to print
def rectangle():

    shape = "rectangle"
    side_one = value_curious(shape, 1)
    side_two = value_curious(shape, 2)

    rec_circumference = calculate_circumference(shape, side_one, side_two)
    rec_area = calculate_area(shape, side_one, side_two)

    print_values(rec_circumference, rec_area)


# Calculates the circumference,
# first_value is side 1 or radius
# second_value is side 2 if it is available
def calculate_circumference(shape_name, first_value, second_value=None):

    circumference = 0
    if shape_name == 'circle':
        circumference = 2 * math.pi * first_value
    elif shape_name == 'square':
        circumference = 4 * first_value
    elif shape_name == 'rectangle':
        circumference = 2 * (first_value + second_value)

    return circumference


# Calculates the area
# first_value is side 1 or radius
# second_value is side 2 if it is available
def calculate_area(shape_name, first_value, second_value=None):

    area = 0
    if shape_name == 'circle':
        area = math.pi * math.pow(first_value, 2)
    elif shape_name == 'square':
        area = math.pow(first_value, 2)
    elif shape_name == 'rectangle':
        area = first_value * second_value

    return area


# Prints the value of circumference and area
def print_values(circumference, area):

    print("The total circumference is {0:0.2f}".format(circumference))
    print("The surface area is {0:0.2f}".format(area))


# Are input values entered by user positive
def is_positive(user_value):

    if user_value > 0:
        status = True
    else:
        status = False

    return status


main()
