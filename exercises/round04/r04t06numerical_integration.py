# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  4.6 Numerical integration
# I learn to pass a function as a parameter to another function.


# Finds an approximation of the area under the curve
def approximate_area(parabel, low_bound, up_bound, num_rec):

    start_point = low_bound

    delta_x = abs(up_bound - low_bound)
    measurement_unit = delta_x/num_rec

    area = 0.0
    counter = 0.0
    greatest_y = 0
    # Finding the maximum value of Y
    while counter <= delta_x:
        y = parabel(start_point)
        if y >= greatest_y:
            greatest_y = y
            greatest_x = start_point
            start_point += measurement_unit
        counter += measurement_unit

    # Calculating the area from lower bound until max Y
    while low_bound < greatest_x:
        y = parabel(low_bound)
        rec_area = y * measurement_unit
        area += rec_area
        low_bound += measurement_unit

    # Calculating the area from upper bound until max Y
    while up_bound > greatest_x:
        y = parabel(up_bound)
        rec_area = y * measurement_unit
        area += rec_area
        up_bound -= measurement_unit

    return area

    print(area)


# Parabola function that returns the y value in each x
def parabel(x):
    return -pow(x, 2) + 2*x + 4


approx_area = approximate_area(parabel, low_bound = -1, up_bound = 3 , num_rec = 16)
print(approx_area)