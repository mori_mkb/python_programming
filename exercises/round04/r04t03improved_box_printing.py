# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  4.3.2 Improved box printing
# Getting to know the parameters named as a topic outside
# these lecture notes (keyword arguments).


def main():
    print_box(5, 4)
    print_box(3, 8, "*")
    print_box(5, 4, "O", "o")
    print_box(inner_mark=".", border_mark="O", height=4, width=6)


# the function prints a filled box based on value that are passed
def print_box(width, height, border_mark="#", inner_mark=" "):

    for i in range(int(height)):
        print()
        for j in range(int(width)):
            if i == 0 or i == height-1:
                print(border_mark, end="")
            else:
                if j == 0 or j == width-1:
                    print(border_mark, end="")
                else:
                    print(inner_mark, end="")
    print()


main()
