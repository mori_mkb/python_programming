# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  4.4 Car
# Learning to implement functions that contain calculations.

from math import sqrt

# This is a text-based menu. You should ONLY touch TODOs inside the menu.
# TODOs in the menu call functions that you have implemented and take care
# of the return values of the function calls.


def menu():
    tank_size  = read_number("How much does the vehicle's gas tank hold? ")
    gas = tank_size  # Tank is full when we start
    gas_consumption = read_number("How many liters of gas does the car " + \
                                 "consume per hundred kilometers? ")
    x = 0.0  # Current X coordinate of the car
    y = 0.0  # Current Y coordinate of the car

    MENU_TEXT =  "1) Fill 2) Drive 3) Quit \n-> "

    while True:
        print("Coordinates x={:.1f}, y={:.1f}, "
              "the tank contains {:.1f} liters of gas.".format(x, y, gas))

        choice = input(MENU_TEXT)

        if choice == "1":
           to_fill = read_number("How many liters of gas to fill up? ")
           gas = fill(tank_size, to_fill, gas)

        elif choice == "2":
           new_x = read_number("x: ")
           new_y = read_number("y: ")
           gas, x, y = drive(x, y, new_x, new_y, gas, gas_consumption)

        elif choice == "3":
           break

    print("Thank you and bye!")


# The function returns one FLOAT that is the amount of gas in the
# tank AFTER the filling up.
def fill(tank_size, to_fill, gas):

    empty_space=float(tank_size-gas)
    if to_fill <= 0:
        return float(gas)
    elif empty_space <= to_fill:
        gas = tank_size
        return float(gas)
    elif empty_space > to_fill:
        gas = gas + to_fill
        return float(gas)


# This function has six parameters. They are all floats.
#   (1) The current x coordinate
#   (2) The current y coordinate
#   (3) The destination x coordinate
#   (4) The destination y coordinate
#   (5) The amount of gas in the tank currently
#   (6) The consumption of gas per 100 km of the car
#
# The parameters have to be in this order.
# The function returns three floats:
#   (1) The amount of gas in the tank AFTER the driving
#   (2) The reached (new) x coordinate
#   (3) The reached (new) y coordinate
#
# The return values have to be in this order.
# The function does not print anything and does not ask for any
# input.
def drive(x, y, new_x, new_y, gas, gas_consumption):
    # It might be usefull to make one or two helper functions to help
    # the implementation of this function (drive)

    consume_per_km = gas_consumption/100
    delta_x = new_x - x
    delta_y = new_y - y
    trip_length = pow(delta_x, 2) + pow(delta_y, 2)
    trip_length = sqrt(trip_length)

    max_trip_length_with_current_gas = gas/consume_per_km

    # This statement will be true if we don't run out of gas until
    # we reach to the destination
    if trip_length <= max_trip_length_with_current_gas:

        gas, x, y = safe_trip(trip_length,
                              consume_per_km,
                              new_x,
                              new_y,
                              x,
                              y,
                              gas)

    # This statement will be true if we run out of gas at the
    # middle of the road and don't reach to the desire destination
    elif trip_length > max_trip_length_with_current_gas:

        gas, x, y = risky_trip(trip_length,
                               max_trip_length_with_current_gas,
                               delta_x,
                               delta_y,
                               x,
                               y,
                               gas)

    return float(gas), float(x), float(y)


# This function calculates the left gas in the tank after reaching to the
# destination and assigns current position of the car
def safe_trip(trip_length, consume_per_km, new_x, new_y, x, y, gas):

    gas_need = trip_length * consume_per_km
    gas = gas - gas_need
    gas = pow(gas, 2)
    gas = sqrt(gas)
    x = new_x
    y = new_y

    return float(gas), float(x), float(y)


# This function calculates the position of car if
# it runs out of gas at the middle of the road
# Right Triangle formulas used in this function
def risky_trip(trip_length, max_trip_length_with_current_gas, delta_x, delta_y, x, y, gas):

    cos_theta = delta_x / trip_length
    sine_theta = delta_y / trip_length
    new_delta_x = cos_theta * max_trip_length_with_current_gas
    new_delta_y = sine_theta * max_trip_length_with_current_gas
    x = x + new_delta_x
    y = y + new_delta_y
    gas = float(0)

    return float(gas), float(x), float(y)


# This function reads input from the user.
def read_number(prompt, error_message="Incorrect input!"):

    try:
        return float(input(prompt))
    except ValueError as e:
        print(error_message)
        return read_number(prompt, error_message)


def main():
    menu()


main()
