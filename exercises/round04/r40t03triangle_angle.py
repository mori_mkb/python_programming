# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task  4.3.1 Triangle's angle
# Get to know default values of parameters and optional parameters.


RIGHT_ANGLE = 90
TOTAL_TRIANGLE_ANGLES = 180


# Calculate the triangle's angle
def calculate_angle(first_angle, second_angle=RIGHT_ANGLE):

    total_two_angles = first_angle + second_angle
    third_angle = TOTAL_TRIANGLE_ANGLES - total_two_angles
    return third_angle
