# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 4.2.2 Comparing floating-point numbers
# Learning to compare floating-point numbers sensibly.


# Error tolerance estimator
def compare_floats(a, b, EPSILON):

    dif = abs(a-b)
    if dif < EPSILON:
        return True
    else:
        return False

