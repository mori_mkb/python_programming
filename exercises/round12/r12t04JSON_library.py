# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 12.4 JSON library

# Learning Goals:
# I practise storing objects to a dict and using lambda function

# importing libraries
import json
import csv

def main():
    jsFileName = input("Enter the name of the input file: ")
    csvFileName = input("Enter the name of the output file: ")

    try:
        with open(jsFileName, "r", encoding="UTF-8") as jsFile:
            jsData = json.load(jsFile)
        with open(csvFileName, "w", newline="", encoding='utf-8') as csvFile:
            writer = csv.writer(csvFile)
            for item in jsData:
                writer.writerow([';'.join([item["stationId"], item["name"]])])

            print()
            print("Conversion succeeded.")

    except(csv.Error, json.JSONDecodeError, IOError):
        print()
        print("There was an error in handling the file.")

main()