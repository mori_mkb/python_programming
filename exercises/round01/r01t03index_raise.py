amount = float(input('Enter the amount of the study benefits: '))
index_raise = float(1.17)
benefit = amount * 1.0117
alter_benefit = benefit * 1.0117
print('If the index raise is', index_raise, 'percent, the study benefit,\n'
    'after a raise, would be', benefit, 'euros\n'
    'and if there was another index raise, the study\n'
    'benefits would be as much as', alter_benefit, 'euros')

