player_one = str(input('Player 1, enter your choice (R/P/S): ')).upper()
player_two = str(input('Player 2, enter your choice (R/P/S): ')).upper()

if player_one == 'S':
    if player_two == 'R':
        print('Player 2 won!')
    elif player_two == 'P':
        print('Player 1 won!')
    else:
        print('It\'s a tie!')
if player_one == 'R':
    if player_two == 'S':
        print('Player 1 won!')
    elif player_two == 'P':
        print('Player 2 won!')
    else:
        print('It\'s a tie!')
if player_one == 'P':
    if player_two == 'R':
        print('Player 1 won!')
    elif player_two == 'S':
        print('Player 2 won!')
    else:
        print('It\'s a tie!')