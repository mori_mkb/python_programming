purchase_price = int(input('Purchase price: '))
paid_amount = int(input('Paid amount of money: '))
change = paid_amount - purchase_price
if change>0:
    print('Offer change:')
    remainder1 = change%10
    if remainder1 >= 0:
        change = change - remainder1
        if change/10>0:
            print(int(change/10), 'ten-euro notes')
    remainder2 = remainder1%5
    if remainder2 >= 0:
        change = remainder1 - remainder2
        if change/5>0:
            print(int(change/5), 'five-euro notes')
    remainder3 = remainder2%2
    if remainder3 >= 0:
        change = remainder2 - remainder3
        if change/3>0:
            print(int(change/2), 'two-euro coins')
    remainder4 = remainder3%1
    if remainder4 >= 0:
        change = remainder3 - remainder4
        if change/1>0:
            print(int(change/1), 'one-euro coins')
else:
    print('No change')