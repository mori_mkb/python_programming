happy = ":-)"
no_feel = ":-|"
sad = ":-("
so_happy = ":-D"
very_sad = ":'("

feeling = int(input('How do you feel? (1-10) '))
if 0 < feeling < 11:
    if 7 < feeling < 10 :
        print("A suitable smiley would be ", happy)
    elif 1 < feeling < 4:
        print("A suitable smiley would be ", sad)
    elif feeling == 1:
        print("A suitable smiley would be ", very_sad)
    elif feeling == 10:
        print("A suitable smiley would be ", so_happy)
    else:
        print("A suitable smiley would be ", no_feel)
else:
    print("Bad input!")