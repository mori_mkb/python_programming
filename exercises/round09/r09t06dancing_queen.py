# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 9.6 Dancing Queen

# Learning Goals:
# Practising the selection and combination of the data structures.


def read_file(filename):
    # reads the played songs and their scores from the file

    try:
        fileobject = open(filename, "r")

        player_result = {}

        # loop over fileobject row by row
        for row in fileobject:
            parts = row.strip().split(";")
            player = parts[0]  # name of the player
            songs = parts[1:]  # songs

            song_scores = {}

            # loop over this player's songs
            for song in songs:
                parts = song.split(":")
                results = parts[1].split(",")
                name = parts[0]  # name of the song
                # list of presses, all integer
                results = [int(luku) for luku in results]

                song_scores[name] = results

            player_result[player] = song_scores

        return player_result

    except IOError:
        print("Error! The file could not be read.")
        return None


def calculate_ratio(scores, coeffs):

    song_score = sum([score*coeff for score, coeff in zip(scores, coeffs)])
    max_song_score = sum(scores) * 5

    return (song_score / max_song_score) * 100


def main():
    coefficients = [5, 4, 2, 0, -6, -12]

    filename = input("Enter the name of the file: ")
    result_info = read_file(filename)

    for player in sorted(result_info):
        print('{0}:'.format(player), end="\n")
        for song in sorted(result_info[player]):
            print('- {0}: {1:0.2f}%'
                  .format(song,
                          calculate_ratio(result_info[player][song],
                                          coefficients)), end="\n")


main()
