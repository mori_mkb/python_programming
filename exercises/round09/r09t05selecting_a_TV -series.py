# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 9.5 Selecting a TV series

# Learning Goals:
# Practising the selection and combination of data structures.


def read_file(filename):
    # reads and saves the series and their genres from the file

    movies_genres = dict()

    try:
        file = open(filename, "r")

        for row in file:
            name, genres = row.rstrip().split(";")
            genres = genres.split(",")

            for genre in genres:
                if genre in movies_genres:
                    movies_genres[genre].append(name)
                if genre not in movies_genres:
                    movies_genres[genre] = []
                    movies_genres[genre].append(name)

        file.close()
        return  movies_genres

    except ValueError:
        print("Error: rows were not in the format name;genres.")
        return None

    except IOError:
        print("Error: the file could not be read.")
        return None


def main():

    filename = input("Enter the name of the file: ")
    genre_list = read_file(filename)

    print('Available genres are: ', end="")
    print(', '.join([genre for genre in sorted(genre_list.keys())]))

    while True:
        genre = input("> ")

        if genre == "exit":
            return

        for gen in genre_list:
            if gen == genre:
                print('\n'.join(sorted([movie for movie in genre_list[gen]])))


main()
