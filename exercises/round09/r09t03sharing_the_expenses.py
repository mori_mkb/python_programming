# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 9.3 Sharing the expenses

# Learning Goals:
# In this project you will be practising how to read files in Python and
# how to organize the read information into an useful data structure.
# The useful data structure in this context means utilizing nested
# data structures. Without nested structures the operations your are
# required to implement will be very difficult.


def main():
    try:
        file_name = str(input("Enter the name of the file: "))
        open_file = open(file_name, 'r')

        # reading the file
        expenses_table, total_cost = read_file(open_file)

        # printing the information
        print_share(expenses_table, total_cost)

        open_file.close()

    except IOError:
        print("Error: file {} cannot be read.".format(file_name))

    except:
        pass


def read_file(file):

    try:
        data_structure = dict()
        total = 0

        # separating and cleaning the info from row file
        for line in file:
            line = line.strip().split(':')
            float_num = float(line[1])
            str_name = str(line[0])
            total += float_num

            # store each person and its expenses one by one
            store_data(str_name, float_num, data_structure)

        return data_structure, total

    except:
        print("Error: there was an erroneous line in the file.")


# Storing the information in a dic\list like this {name:[expense1,expense2]}
def store_data(name, amount, database):

    if name in database:
        database[name].append(amount)
    if name not in database:
        database[name] = []
        database[name].append(amount)


# Printing the share and expenses for each person
def print_share(database, total):

    print('Total costs: {0:0.2f}e'.format(total))

    # calculating the equal share for all
    equal_share = total/len(database)

    # printing the information in an alphabetical order
    for name in sorted(database):
        print()
        print('{0} has paid {1:0.2f} in the following amounts: '
              .format(name, sum(database[name])), end="")

        # PYTHONIC way is awesome
        print(", ".join([format(paid, '0.2f') for paid in database[name]]))

        # calculate the difference between all expenses and equal share
        diff = sum(database[name]) - equal_share

        # ignore small coins :D
        if 0.05 > diff > -0.05:
            print('No transfers needed.')

        # you paid more
        elif sum(database[name]) > equal_share:
            print('{0} needs to receive {1:0.2f}e.'
                  .format(name, (sum(database[name]) - equal_share)))

        # you paid less
        elif sum(database[name]) < equal_share:
            print('{0} needs to pay {1:0.2f}e.'
                  .format(name, (equal_share - sum(database[name]))))


main()

