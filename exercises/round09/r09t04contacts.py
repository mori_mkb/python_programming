# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 9.4 Contacts

# Learning Goals:
# I learn to use a dict to combine pieces of information that are
# related to each other.


def read_file(file):
    try:
        open_file = open(file, "r")
        field = open_file.readline().rstrip()
        field = field.split(';')
        contacts = dict()

        for line in open_file:
            if not line.startswith('key'):
                line = line.rstrip().split(";")
                if len(line) == 4:
                    contacts[line[0]] = {field[1]: line[1],
                                         field[2]: line[2],
                                         field[3]: line[3]}
                elif len(line) == 5:
                    contacts[line[0]] = {field[1]: line[1],
                                         field[2]: line[2],
                                         field[3]: line[3],
                                         field[4]: line[4]}
        return contacts

    except:
        print("There are some issues.")


#print(read_file('contacts.csv'))
