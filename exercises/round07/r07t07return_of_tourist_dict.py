# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.7.3 The return of the tourist dictionary

# Learning Goals:
# I get familiar with lambda functions.


# Prints the spanish to english dictionary without creating another dict
def print_in_accordance_of_values(english_spanish_dict):
    # Using lambda function, getting values by using that and sort them
    new_dict = sorted(english_spanish_dict.items(), key=lambda item: item[1])
    for key, value in new_dict:
        print(value, key)


english_spanish = {"hi": "hola", "thanks": "gracias", "yes": "si", "no": "no"}
print_in_accordance_of_values(english_spanish)
