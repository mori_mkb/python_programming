# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.6 Contacts

# Learning Goals:
# I learn to use a dict to combine pieces of information
# that are related to each other.


def main():
    contacts = {"Tom": {"name": "Tom Techie",
                        "phone": "040 123546",
                        "email": "tom@tut.fi",
                        "skype": "tom_92_"},
                "Mike": {"name": "Mike Mechanic",
                         "phone": "050 123546",
                         "email": "mike@tut.fi",
                         "skype": "-Mike-M-"},
                "Archie": {"name": "Archie Architect",
                           "phone": "050 987654",
                           "email": "archie@tut.fi"}}

    contact = input("Enter the name of the contact: ")
    field = input("Enter the field name you're searching for: ")

    # Finds the persons contact info and prints it
    find_person(contacts, contact, field)


def find_person(contacts, contact_val, field_val):
    """ Finds the persons contact info and prints it
    :param contacts: dict, contact dictionary
    :param contact_val: the persons first name
    :param field_val: phone or email or skype
    """
    # lets see the person exists in our dict
    if contact_val in contacts:

        # lets see he/she has user input contact info
        if field_val in contacts[contact_val]:
            print("{0}, {1}: {2}".format(contacts[contact_val]["name"],
                                         field_val,
                                         contacts[contact_val][field_val]))

        # if his/her contact info does not exist then print the message
        if field_val not in contacts[contact_val]:
            print("No {} for {}".format(field_val,
                                        contacts[contact_val]['name']))

    # if he/she does not exist then print message
    if contact_val not in contacts:
        print("No contact information for", contact_val)


main()
