# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.4 Word density calculator

# Learning Goals:
# Also using dict for saving the amounts and to execute operations where
# information contained by dict is used for calculation.


def main():
    """ Gets user text, calculate words density, print words density
    """

    user_text = get_text()  # Gets user text
    word_density_dict = word_count(user_text)  # Calculate words density
    print_word_density(word_density_dict)  # Print words density


def get_text():
    """ Gets the text from user and add it to the a string
    :return: str, a string
    """

    full_text = ""
    print("Enter rows of text for word counting. Empty row to quit.")
    text = input()
    while text != "":
        text += " "  # adding a space at the end of each user input line
        full_text += text
        text = input()

    return full_text


def word_count(text):
    """ Find the words density of user input text
    :param text: str, user input text
    :return: dict, words as key and density as value
    """

    # create a new dictionary
    words_density = dict()

    # make the text lower first and then clean spaces and split them by spaces
    words = text.lower().strip().split(" ")

    # sorts the words and find the density of them
    # new word gets value of 1
    for word in sorted(words):
        words_density[word] = words_density.get(word, 0) + 1

    return words_density


def print_word_density(word_density_dict):
    """ Prints the words density from the dictionary
    :param word_density_dict: dict, sorted words and their density
    """

    for key, val in word_density_dict.items():
        print(key, ":", val, "times")


main()


# Another way to get word density
"""
def word_count(text):
    words_density = dict()
    text = text.lower().strip().split(" ")
    for word in sorted(text):
        if word not in words_density:
            words_density[word] = 1
        else:
            words_density[word] = words_density[word] + 1

    return words_density
"""
