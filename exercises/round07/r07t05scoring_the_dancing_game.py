# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.5 Scoring the dancing games

# Learning Goals:
# Getting acquainted with Python's documentation for dict.


# CONSTANT VALUES
SONG_RESULT = {"Bubble dancer": 93.4, "The Game": 92.03, "Vertex": 75.3,
               "Lemmings on the Run": 86.2, "Da Roots": 96.02,
               "Charlene": 75.3, "Disconnected": 86.3, "Fly away": 87.32,
               "Hybrid": 63.9, "My favourite game": 89.45, "Oasis": 59.5,
               "Remember December": 96.3, "The beginning": 90.45,
               "Tribal Style": 87.45, "Why Me": 97.38, "Xuxa": 63.84,
               "Zodiac": 83.43, "Queen of Light": 75.12, "Mouth": 98.34,
               "Pandemonium": 79.31}


def main():
    """ Call calculate function and prints it
    """

    print(calculate_average(SONG_RESULT))


def calculate_average(song_res):
    """ Calculate the average
    :param song_res: dict, list of songs and their results
    :return: float, average of all results
    """

    total = sum(song_res.values())
    dic_length = len(song_res)
    average = float(total / dic_length)

    return average


main()
