# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.2.1 A tourist dictionary

# Learning Goals:
# First, I review the basic operations of a dictionary (dict) and, in the
# last last few sections, also some operations
# that are somewhat more complicated.


def main():

    # english to spanish dictionary
    english_spanish = {"hey": "hola", "thanks": "gracias", "home": "casa"}

    # print dictionary contents
    dic_content(english_spanish)

    # gets the command from user and do the command
    while True:
        command = input("[W]ord/[A]dd/[R]emove/[P]rint/[T]ranslate/[Q]uit: ")

        if command == "W":
            word_translator(english_spanish)

        elif command == "A":
            add_word(english_spanish)
            dic_content(english_spanish)

        elif command == "R":
            remove_word(english_spanish)

        elif command == "P":
            print_word(english_spanish)

        elif command == "T":
            text_translator(english_spanish)

        elif command == "Q":
            print("Adios!")
            return

        else:
            print("Unknown command, enter W, A, R, P, T or Q!")


def word_translator(english_spanish):
    """ Translate an english word from english to spanish
    :param english_spanish: dict, A dictionary
    """

    word_sp = input("Enter the word to be translated: ")
    try:
        word_en = english_spanish[word_sp]
        print(word_sp, "in Spanish is", word_en)
    except KeyError:
        print("The word", word_sp,
              "could not be found from the dictionary.")


def add_word(english_spanish):
    """ Adds an english word and its spanish translation to the dictionary
    :param english_spanish: dict, A dictionary
    """

    word_en = input("Give the word to be added in English: ")
    word_sp = input("Give the word to be added in Spanish: ")
    english_spanish[word_en] = word_sp


def remove_word(english_spanish):
    """ Remove an english word and its translation from dictionary
    :param english_spanish: dict, A dictionary
    """

    word_sp = input("Give the word to be removed: ")
    try:
        del english_spanish[word_sp]
    except KeyError:
        print("The word", word_sp,
              "could not be found from the dictionary.")


def print_word(english_spanish):
    """ Prints a sorted dictionary of english to spanish and spanish to english
    :param english_spanish: dict, A dictionary
    """

    # prints all sorted words and their meaning in english to spanish dict
    print()
    print("English-Spanish")
    for key in sorted(english_spanish):
        print(key, english_spanish[key])

    # create a new dictionary for spanish to english
    spanish_english = dict()
    # put all items into the new dictionary but
    # turn key to value and vice versa
    for key, val in english_spanish.items():
        spanish_english[val] = key

    # prints all sorted words and their meaning in spanish to english dict
    print()
    print("Spanish-English")
    for key in sorted(spanish_english):
        print(key, spanish_english[key])
    print()


def text_translator(english_spanish):
    """ Translate a text from english to spanish
    :param english_spanish: dict, A dictionary
    """

    # getting the text
    text = input('Enter the text to be translated into Spanish: ')
    sp_text = "" # create an empty string
    eng_words = text.split(' ')  # Split the text to words

    # if word is in our dictionary then add the translation of it
    # to the list, otherwise add the word itself to the string
    for word in eng_words:
        if word in english_spanish:
            sp_text += english_spanish[word]
            sp_text += ' '
        if word not in english_spanish:
            sp_text += word
            sp_text += ' '

    # print the translated text
    print("The text, translated by the dictionary:")
    print(sp_text)


def dic_content(english_spanish):
    """ Print the english words in dictionary in alphabetical order
    :param english_spanish: dict, A dictionary
    """

    delimiter = ", "
    print("Dictionary contents:")
    print(delimiter.join(sorted(english_spanish.keys())))


main()
