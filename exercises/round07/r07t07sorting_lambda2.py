# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.7.2 Sorting numbers in an alphabetical order

# Learning Goals:
# I get familiar with lambda functions.


def sort_integers_alphabetically(integers):
    return sorted(integers, key=lambda num: str(num))


# nums = [10, 1, 101, 2, 111, 212, 100000, 22, 222, 112, 10101, 1100, 11, 0]
# print(sort_integers_alphabetically(nums))