# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 7.7.1 Sorting that neglects the case of characters

# Learning Goals:
# I get familiar with lambda functions.


# vehicles = ["Mercedes", "BMW", "Lada", "car", "Audi", "automobile"]
# change them to lower and then compare them for sorting

# Sort alphabetically, first capital char and ten same char lower case
def sort_neglecting_case(vehicles):
    return sorted(vehicles, key=lambda word: word.lower())
