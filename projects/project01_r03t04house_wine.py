# TIE-02106 Introduction to Programming
# morteza.mohammadkhanbeigi@student.tut.fi, student nr: 267935
# Solution of task 3.4 House Wine / PROJECT 1
# To familiarize the student with control structures (if and while) and
# the roles of variables in programs.


# Constant Values
LOWEST_TEMPERATURE = 20
GREATEST_TEMPERATURE = 25
MAX_ALLOWED_OUTSIDE_RANGE = 2
MAX_ALLOWED_PERCENTAGE = 0.1


# The function asks total number of wine measurements
def main():

    measurements = int(input("Enter the number of measurements: "))
    if is_positive(measurements):
        if ask_temperature(measurements):
            print("Your wine is ready.")
        else:
            print("Your wine is ruined.")


# The function asks the temperature of each measurement and
# return false:
#   2 consecutive measurements are outside range
#   or more than 10% of the total measurements is outside range
# return True:
#   otherwise
def ask_temperature(total_measures):

    measurements_counter = 1
    wrong_temp = 0
    allowed_percentage = total_measures * MAX_ALLOWED_PERCENTAGE
    allowed_mistake = MAX_ALLOWED_OUTSIDE_RANGE
    while total_measures >= measurements_counter:
        if (allowed_mistake == 0) or wrong_temp > allowed_percentage:
            return False
        else:
            temperature = int(input(
                "Enter the temperature " + str(measurements_counter) + ": "))
            if check_temperature(temperature):
                allowed_mistake = MAX_ALLOWED_OUTSIDE_RANGE
            else:
                wrong_temp += 1
                allowed_mistake -= 1
        measurements_counter += 1

    return True


# Checks temperature value
# 20<=temperature<=25 ========>>> accepted temperature
def check_temperature(temp_value):

    if LOWEST_TEMPERATURE <= temp_value <= GREATEST_TEMPERATURE:
        status = True
    else:
        status = False
    return status


# Is input value entered by user is greater than 0 or not?
def is_positive(measurement_value):

    if measurement_value > 0:
        status = True
    else:
        print("The number of measurements must be a positive number.")
        status = False
    return status


main()
