# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Name: Mohammadali Akbarisamani, Student Number: 256270
# Email: akbarisa@student.tut.fi
# Solution of task 13.6 Design and implementation of a graphical user interface
# PROJECT 5

# Learning Goals:
# I design a program with a graphical user interface and implement it using the
# Tkinter library.

# This is a Hangman gam that user enter one word or one letter to guess a word
# that program provide for him/her. The game is scalable in some aspects such
# as words length that are saved in a list, also buttons are scalable but
# they need some modification for grid method, we believe that pack would be
# better for this project but since the course used grid then we used them.
# Pictures are made by paint 3D and only letter buttons are found from internet

######################################################################
######################################################################
############      THANKS TO ALL The COURSE STAFF      ################
############   SPECIALLY ASSISTANTS THAT HELPED US    ################
############          A LOT IN THIS COURSE            ################
######################################################################
######################################################################


# Import needed libraries
from tkinter import *
import random
import os.path

# Constant Values
HANGERPICS = ["1.gif",
              "2.gif",
              "3.gif",
              "4.gif",
              "5.gif",
              "6.gif",
              "7.gif",
              "8.gif",
              "9.gif",
              "10.gif"]

ALPHABETS = ["A.gif", "B.gif","C.gif","D.gif","E.gif","F.gif","G.gif","H.gif",
             "I.gif","J.gif","K.gif","L.gif","M.gif","N.gif","O.gif","P.gif","Q.gif",
             "R.gif","S.gif","T.gif","U.gif","V.gif","W.gif","X.gif","Y.gif",
             "Z.gif", "EXTRA1.gif", "EXTRA2.gif"]

WORDS = ['Iran', 'Germany', 'Finland', "Web", "Python", "Introduction",
         "Dictionary", "Version", "Console", "Cyprus", "Debug", "Run",
         "University", "Simulator", "Game", "Network", "Hangman", "Algorithm"]

EXTRA_ALPHABETS = 2

IMAGES_FOLDER_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "images")

class HangmanGUI:
    # This class provides game GUI and it's methods
    def __init__(self):
        self.__window = Tk()
        self.__window.title("Hangman")
        self.__window.option_add("*Font", "Verdana 12")
        self.__window.config(background="black")
        self.__words = WORDS
        self.__word = self.random_word_generator()
        self.__guesses = []
        self.__guess = ''
        self.__point = 0
        self.__hearts = 9

        self.__hangerPics = []
        for picfile in HANGERPICS:
            pic = PhotoImage(file=os.path.join(IMAGES_FOLDER_PATH, picfile))
            self.__hangerPics.append(pic)

        self.__alphabetPics = []
        for alphabetPic in ALPHABETS:
            pic = PhotoImage(file=os.path.join(IMAGES_FOLDER_PATH, alphabetPic))
            self.__alphabetPics.append(pic)

        self.__heading = Label(self.__window, font="Verdana 40 bold",
                               bg = "black",fg="white", text="HANGMAN")
        self.__heading.grid(row=0, column= 2, columnspan=5, sticky=E + W)

        self.__entryLabel = Label(self.__window,font="Verdana 11 bold",
                                  bg = "black",fg="white",
                                  text="Guess {} letters word or only 1 letter"
                                  .format(len(self.__word)))
        self.__letterEntry = Entry(self.__window, width = 30)
        self.__entryLabel.grid(row = 1, column= 4, padx=10,
                               sticky =E + W + N + S)
        self.__letterEntry.grid(row=2, column=4, padx=10)

        self.__wordLabel = Label(self.__window,font="Verdana 11 bold",
                                bg="light gray", text=['*'] * len(self.__word))
        self.__wordLabel.grid(row=3, column=4, padx=10, sticky=E + W + N + S)

        # This might be scalable but we could not find better solution for it
        counter = 0
        self.__alphabetButtons = []
        for i in range(2 , 9):
            for j in range(0, 4):
                def button_press(index=counter):
                    self.enter_letter(index)
                if counter >= len(ALPHABETS) - EXTRA_ALPHABETS:
                    new_letterButton = Button(self.__window,
                                              image=self.__alphabetPics[
                                                  counter],
                                              bg="black",
                                              state = DISABLED,
                                              command=button_press)
                    new_letterButton.grid(row=i, column=j, padx=10)
                    self.__alphabetButtons.append(new_letterButton)
                    counter += 1

                else:
                    new_letterButton = Button(self.__window,
                                              image=self.__alphabetPics[counter],
                                              bg="black", command=button_press)
                    new_letterButton.grid(row=i, column=j, padx= 10)
                    self.__alphabetButtons.append(new_letterButton)
                    counter += 1

        self.__pointLabel = Label(self.__window, bg="white",
                                  font="Verdana 11 bold",
                                  text="Points: {0}".format(self.__point))
        self.__pointLabel.grid(row=4, column=4,padx=10,pady=0,
                               sticky=E + W + S + N)

        self.__heartsLabel = Label(self.__window,font="Verdana 11 bold",
                                   bg="light gray",
                                   text = "Hearts: {0}".format(self.__hearts))
        self.__heartsLabel.grid(row=5, column=4, padx=10, pady=0,
                               sticky=E + W + S + N)

        self.__descLabel = Label(self.__window, bg="white",
                                 font="Verdana 10 bold italic",
                                 text="Press on Try after guessing")
        self.__descLabel.grid(row=6, column=4,padx=10,pady=0,
                              sticky=E + W + S + N)

        self.__picLabel = Label(self.__window, image = self.__hangerPics[9])
        self.__picLabel.grid(row=1, column=5, columnspan=3, rowspan=9,
                             sticky=W+E+N+S, padx=20, pady=30)

        self.__tryButton = Button(self.__window,bg="Green", text="Try",
                                  command=self.get_user_guess)
        self.__tryButton.grid(row=7, column=4, padx=10,pady=0, sticky=W + E)

        self.__resetButton = Button(self.__window, bg="yellow", text="New Game"
                                    , command=self.initialize_game)
        self.__resetButton.grid(row=8, column=4, padx=10,pady=0, sticky=W + E)

        self.__quitButton = Button(self.__window, bg="Red", text="Quit",
                                   command=self.quit)
        self.__quitButton.grid(row=9, column=4, padx=10,pady=0, sticky=W + E)

    def start(self):
        self.__window.mainloop()

    def quit(self):
        self.__window.destroy()

    def initialize_game(self):
        # This method creates a new game whenever new game button is clicked
        self.__point = 0
        self.__word = self.random_word_generator()
        self.__guesses = []
        self.__hearts = 9
        self.__entryLabel.configure(text="Guess {} letters word or only 1 "
                                         "letter".format(len(self.__word)))
        self.__descLabel.configure(font="Verdana 10 bold italic",
                                   text="Press on Try after guessing")
        self.__wordLabel.configure(text=['*'] * len(self.__word))
        self.__letterEntry.delete(0, END)
        self.__pointLabel.configure(
            text="Points: {0}".format(self.__point))
        self.__heartsLabel.configure(text="Hearts: {0}".format(self.__hearts))
        self.__tryButton.configure(state=NORMAL)
        self.__picLabel.configure(image=self.__hangerPics[self.__hearts])

    def random_word_generator(self):
        # return a random word from words list
        word = random.choice(self.__words).upper()
        return word

    def get_user_guess(self):
        # receives user input, if input is 0 then it reject it, if input
        # is accepted then it sends the word for check_word method,
        # it also saves the words or letters that are already entered by user
        self.__guess = self.__letterEntry.get()
        if len(self.__guess) == 0:
            self.__descLabel.configure(text="You must enter a letter or a word")
            self.__letterEntry.delete(0, END)
            return

        self.__guess = self.__guess.upper()
        if self.__guess in self.__guesses:
            self.__descLabel.configure(
                text="You already guessed {}".format(self.__guess))
            self.__letterEntry.delete(0, END)
        else:
            self.check_word()

    def check_word(self):
        # It checks if the input is a letter or word, it also checks the word
        # is the correct word or not
        if len(self.__guess) == len(self.__word):
            self.__guesses.append(self.__guess)
            if self.__guess == self.__word:
                self.next_word()
            else:
                self.decrease_heart()

        elif len(self.__guess) == 1:
            self.__guesses.append(self.__guess)
            self.check_letter()
        else:
            self.decrease_heart()

    def check_letter(self):
        # Checks the letter entered by user is correct and available in program
        # word or not
        status = []
        matches = 0
        for letter in self.__word:
            if letter in self.__guesses:
                status.append(letter)
            else:
                status.append('*')
            if letter == self.__guess:
                matches += 1

        if matches > 1:
            self.__descLabel.configure(
                text="Yes, The word contains {0} {1}'s"
                    .format(matches, self.__guess))
        elif matches == 1:
            self.__descLabel.configure(
                text="Yes, The word contains the letter {0}"
                    .format(self.__guess))
        else:
            self.__descLabel.configure(
                text="The word does not contain the letter {}"
                    .format(self.__guess))
            self.decrease_heart()

        self.__wordLabel.configure(text=status)
        self.__letterEntry.delete(0, END)

        if ''.join(status) == self.__word:
            self.next_word()

    def next_word(self):
        # if user guess the word correctly then next word will be shown by this
        # method
        self.__descLabel.configure(
            text="Congrats!,\nyou got the word in {0} tries"
                .format(len(self.__guesses)))
        self.__point += 1
        self.__letterEntry.delete(0, END)
        self.__pointLabel.configure(text="Points: {0}".format(self.__point))
        self.__heartsLabel.configure(text="Hearts: {0}".format(self.__hearts))
        self.__guesses = []
        self.__word = self.random_word_generator()
        self.__wordLabel.configure(text=['*'] * len(self.__word))
        self.__entryLabel.configure(text="Guess {} letters word or only 1 "
                                         "letter".format(len(self.__word)))

    def decrease_heart(self):
        # if user input is wrong then one of heart will be decreased by this
        # method. if no heart then program ends by deactivating the Try button
        # and the player is hanged !!!!!
        self.__hearts -= 1
        self.__letterEntry.delete(0, END)
        if self.__hearts == 0:
            self.__picLabel.configure(image=self.__hangerPics[self.__hearts])
            self.__descLabel.configure(font="Verdana 9 bold italic",
                                       text="FATALITY, You guessed {} words\n"
                                            " Click on New Game for new match"
                                       .format(self.__point))

            self.__pointLabel.configure(text="Points: {0}".format(self.__point))
            self.__heartsLabel.configure(
                text="Hearts: {0}".format(self.__hearts))
            self.__tryButton.configure(state=DISABLED)

        else:
            self.__picLabel.configure(image=self.__hangerPics[self.__hearts])
            self.__descLabel.configure(font="Verdana 9 bold italic",
                text="WRONG\nYou are {0} step away to get executed".format(
                    self.__hearts))
            self.__pointLabel.configure(text="Points: {0}".format(
                self.__point))
            self.__heartsLabel.configure(
                text="Hearts: {0}".format(self.__hearts))

    def enter_letter(self, index):
        # Inserts letter to the entry field by pressing on letter button
        self.__letterEntry.insert(END, ALPHABETS[index].split(".")[0])


def main():

    ui = HangmanGUI()
    ui.start()


main()




