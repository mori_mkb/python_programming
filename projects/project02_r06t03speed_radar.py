# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 6.3 Speed radar by the school / PROJECT 2

# Learning to divide my program into functions and to use parameters and
# return values to pass information between the caller and the function.
# Understanding how to save data in a list.

# This program helps to save and calculate the statistics of driver's speed
# such as the difference between maximum speed and minimum speed,
# median of speeds and speeders who broke the maximum
# allowed speed in one specific location.


# Constant values
REGISTERED_SPEED = 20
MIN_SPEED_LIMIT_BY_LAW = 8
MAX_SPEED_LIMIT_BY_LAW = 20


def main():

    speed_limit = int(input("Enter the speed limit for "
                            "the measurement location: "))

    if is_positive(speed_limit):
        # Receives speeds above reg speed(20) and put them in a database(list)
        measured_speeds, reg_speed_counter = get_speeds()

        if reg_speed_counter > 0:
            # Print the ignored speeds
            print("Ignored {0} measurement results whose values were "
                  "under {1} km/h".format(reg_speed_counter, REGISTERED_SPEED))

        if len(measured_speeds) > 0:
            # Calculating and representing statistical information
            calculate_statistics(measured_speeds, speed_limit)


def get_speeds():
    """ Create a list from measured speeds greater than registered speed(20)
    :return: list:measured speeds, int:reg speed counter
    """

    measurement_table = list()
    counter = 0
    print("Enter the results of the speed "
          "measurements, finish with an empty line:")
    speed_measurement = input()  # Enter speeds
    while speed_measurement != "":
        speed_measurement = int(speed_measurement)
        # Accept the speeds that are above registered speed(20) or equal
        if speed_measurement >= REGISTERED_SPEED:
            measurement_table.append(speed_measurement)  # Add to the list
        else:
            counter += 1  # Count the speed under registered speed
        speed_measurement = input()  # Enter speeds
    return measurement_table, counter


def calculate_statistics(measurement_values, speed_limit_value):
    """ Calculating statistical information and sends them to print
    :param measurement_values: list, all speeds greater than registered speed
    :param speed_limit_value: int, location speed limit
    :return: None
    """

    # Find range and print it
    range_value = find_range(measurement_values)
    print_statistics(1, range_value)

    # Find median and print it
    median_value = find_median(measurement_values)
    print_statistics(2, median_value)

    # Lets find those who broke the law!
    speeders, count_speed_tickets, count_courts = find_speeders(
        measurement_values, speed_limit_value)
    # Represent the number of law breaker
    print_statistics(3, count_speed_tickets)
    print_statistics(4, count_courts)
    # Oh, oh, Represent the law breakers one by one
    print_statistics(5, len(speeders), speeders)


def find_speeders(measurement_values, speed_limit_value):
    """ Find the law breakers who drove above speed limit
    :param measurement_values: list, all speeds greater than registered speed
    :param speed_limit_value: int, the location speed limit
    :return: list:all speeders list,
             int:number of on-the-spot tickets,
             int:number of drive who introduced to the courts
    """

    punished_drivers = list()  # Create a new list to save speeders
    speed_ticket_counter = 0
    court_counter = 0
    # Calculate the range of speed limits for a specific location
    min_speed_limit = int(speed_limit_value + MIN_SPEED_LIMIT_BY_LAW)
    max_speed_limit = int(speed_limit_value + MAX_SPEED_LIMIT_BY_LAW)

    # Checks all speeds to find the speeders
    for measure in measurement_values:
        # Find the speeders who gets only ticket as fine
        if min_speed_limit <= measure < max_speed_limit:
            speed_ticket_counter += 1  # One more ticket issued for one speeder
            punished_drivers.append(measure)  # Added to the list of speeders
        # Find the speeders who introduce to the court
        elif measure >= max_speed_limit:
            court_counter += 1  # One more speeder introduced to the court
            punished_drivers.append(measure)  # Added to the list of speeders

    return punished_drivers, speed_ticket_counter, court_counter


def find_range(measurement_values):
    """ Calculate the difference between max speed and min speed
    :param measurement_values: list, all speeds greater than registered speed
    :return: int, the value of the calculated difference, 0 if len < 2
    """

    difference = 0
    if len(measurement_values) > 1:
        # Calculate the difference
        minimum_value = min(measurement_values)
        maximum_value = max(measurement_values)
        difference = maximum_value - minimum_value

    return difference


def find_median(measurement_values):
    """ Calculate the the median value of all speeds
    :param measurement_values: list, all speeds greater than registered speed
    :return: float, the median value based on length of the list, 0 if len < 2
    """

    median = 0.0
    # Create new list and copy sorted list in it
    sorted_list = sorted(measurement_values)
    list_length = len(sorted_list)
    if is_even(list_length):
        # even_length_median: ((len/2)+(len/2)-1) / 2
        right_index = int(len(sorted_list) / 2)
        left_index = int(right_index - 1)
        # Calculate the median if length is even
        median = (sorted_list[left_index] + sorted_list[right_index])/2

    if is_odd(list_length):
        # odd_length_median: (integer(len/2)) / 2
        median_index = int(len(sorted_list)/2)
        # Calculate the median if length is odd
        median = float(sorted_list[median_index])

    return median


def print_statistics(option, value, value_list=None):
    """ prints the statistics if their value is available (not zero)
    :param option: int,selection of text based on call function
    :param value: int or float, calculated value for each stats
    :param value_list: list, list of speeders if available
    :return: None
    """

    text = ""
    if option == 1:
        text = "Range: " + str(value)
    elif option == 2:
        text = "Median: " + str(value)
    # Prints only when there is at least one speeder
    if value > 0:
        if option == 3:
            text = "The on-the-spot-fine for speeding would " \
                   "have been issued to {} drivers".format(value)
        elif option == 4:
            text = "The fine for speeding would have been " \
                   "issued to {} drivers".format(value)
        elif option == 5:
            print("Speeders in the order of measurements:")
            for driver in value_list:
                print(driver, sep="")
    if text != "":
        print(text)  # Prints text based on options


def is_positive(speed_limit_value):
    """ Check if value is positive or not
    :param speed_limit_value: int, the location speed limit
    :return: bool, True if value is positive, False otherwise
    """

    if speed_limit_value > 0:
        status = True
    else:
        print("The speed limit must be expressed as a positive integer.")
        status = False
    return status


def is_even(length_value):
    """ Checks if value is even
    :param length_value: length of the measured speed list
    :return: bool, True if length value mod by 2 is equal to 0, False otherwise
    """

    if length_value % 2 == 0:
        status = True
    else:
        status = False
    return status


def is_odd(length_value):
    """ Checks if value is odd
    :param length_value: length of the measured speed list
    :return: bool, True if length value mod by 2 is equal to 1, False otherwise
    """

    if length_value % 2 == 1:
        status = True
    else:
        status = False
    return status


main()


# -----MORE EFFICIENT WAY TO CHECK WHETHER A VALUE IS ODD OR NOT-----
# *****BITWISE*****
# def is_odd(length_value):
#    return bool(length_value & 1)
