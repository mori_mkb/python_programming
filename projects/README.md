# 1. House Wine (project01_r03t04house_wine)

- Making house wine is an exact work: the temperature of the wine must be closely controlled at least 
  most of the time. Let's implement a program which reads the temperature measurements of the wine 
  container during the fermentation process and tells based on those measurements whether the wine is ruined or not. 


# 2. Speed radar by the school (project02_r06t03speed_radar)

- An elementary school's parents' association is upset and worried over there not being any 
  traffic lights, or even speed bumps, on the road to the school. Now the parental association 
  has placed their own speed radar by the road, and they want to use its measurements for calculating 
  statistics of interest to support their demands for organizing the traffic passing the school.

- Their radar equipment is able to measure speed only to the accuracy of 1 km/h, which means that all the 
  measurements can be handled as integers.

- In this task, we will create a program which aids the parental association by 
  performing the most difficult(?) calculations. 
  
  
# 3. Friendship Network (project03_r09t06friendship_network)

- In this project I will be implementing a little helper program which
  allows you to analyze the dynamics related to human relationships.
  The relationships (i.e. friendships) the program understands
  can be expressed with a relationship graph


# 4. Routing protocol simulator (project04_r12t03routing_protocol_simulator)

- In this task we implement a program that a student of computer networking 
  could use to study how the data in the routing tables of routers change when 
  the device communicate with each other.

- Router is a device, that routs data connections to networks it knows or other 
  routers that will do the same, ie. route the data connections further. 
  A router has connections to its neighbour routers and a routing table, that 
  contains the distances to different networks.
  
  
# 5. Graphical User Interface: Hangman Game (project05_r13t06hangman.py)

- This is a famous Hangman game that it is implemented in python with "Tkinter library".

- letter's images are downloaded from internet but big images(hanger, chair, troll, ...) are designed in paint 3D.