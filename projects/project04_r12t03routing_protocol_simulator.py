# TIE-02106 Introduction to Programming
# Name: Morteza Mohammadkhanbeigi, Student Number: 267935
# Email: morteza.mohammadkhanbeigi@student.tut.fi
# Solution of task 12.3 Routing protocol simulator / PROJECT 4

# Learning Goals:
# I practise defining a class interface by myself and using objects.

# This program simulate the behavior of the device "Router" and
# shows how the data in routing table of router changes when
# router communicate with other routers

# Constant values
EDGE_ROUTER_DISTANCE = 0


class Router:
    """
    This class creates Router Objects and handles the behaviour of them by
    providing proper methods.
    """

    def __init__(self, name):
        """ Constructing and object with private attributes
        :param name: (str) router's name
        """

        self.__name = name
        self.__neighbours = []
        self.__routingTable = {}

    def print_info(self):
        """ Representing all routers's information
        :return: None
        """

        print(f"  {self.__name}")
        print(f"    ", end="")
        print(f"N: {', '.join(sorted(self.__neighbours))}")
        print(f"    ", end="")
        print(f"R: {', '.join('{0}:{1}'.format(net, dist) for net, dist in sorted(self.__routingTable.items()))}")

    def add_neighbour(self, anotherRouter):
        """ Creates a relationship between router and anotherRouter by neighbouring them
        :param anotherRouter: (:obj:`Router`) neighbour object
        :return: None
        """

        if anotherRouter.__name not in self.__neighbours:
            self.__neighbours.append(anotherRouter.__name)

    def add_network(self, netName, distance):
        """ Adds route to the routing table
        :param netName: (str) network name
        :param distance: (int) distance of the router to specific network
        :return: None
        """

        if netName not in self.__routingTable:
            self.__routingTable[netName] = distance

    def receive_routing_table(self, anotherRouter):
        """ Receives roting table of main router when neighbour router calls it
        :param anotherRouter: anotherRouter: (:obj:`Router`) main router object
        :return: None
        """

        for net, dist in anotherRouter.__routingTable.items():
            # distance will be increased and the method will be called
            # if route wont be available in routing table then route will be
            # added with increased distance, otherwise the route wont be added.
            self.add_network(net, dist+1)

    def get_neighbours(self):
        """ Return the list of neighbours for one object
        :return: (list)
        """

        return self.__neighbours

    def has_route(self, netName):
        """ Represents the route information of specific network
        :param netName: (str) Network name
        :return: None
        """

        if netName in self.__routingTable:
            if self.__routingTable[netName] == EDGE_ROUTER_DISTANCE:
                print("Router is an edge router for the network.")
            else:
                print(f"Network {netName} is {self.__routingTable[netName]} "
                      f"hops away")
        else:
            print("Route to the network is unknown.")


def main():

    routerfile = input("Network file: ")

    # Create a dictionary, key=Router Name, value=Router Object
    routers = {}

    # File processing in case of entered file name
    # -- any errors results in stopping the program
    if len(routerfile) != 0 and not add_routers_from_file(routers, routerfile):
        return

    # Command line processing
    while True:
        command = input("> ")
        command = command.upper()

        if command == "P":
            router = input("Enter router name: ")
            if router in routers:
                routers[router].print_info()
            else:
                print("Router was not found.")

        elif command == "PA":
            for router in routers:
                routers[router].print_info()

        elif command == "S":
            router = input("Sending router: ")
            if router in routers:
                for neighbour in routers[router].get_neighbours():
                    routers[neighbour].receive_routing_table(routers[router])

        elif command == "C":
            firstRouter = input("Enter 1st router: ")
            secRouter = input("Enter 2nd router: ")
            add_new_neighbours(routers, firstRouter, [secRouter])

        elif command == "RR":
            router = input("Enter router name: ")
            netName = input("Enter network name: ")
            if router in routers:
                routers[router].has_route(str(netName))

        elif command == "NR":
            router = input("Enter a new name: ")
            add_new_router(routers, router)

        elif command == "NN":
            router = input("Enter router name: ")
            if router in routers:
                netName = input("Enter network: ")
                distance = input("Enter distance: ")
                add_new_routes(routers, router, [('{0}:{1}'.format(netName,
                                                                   distance))])
        elif command == "Q":
            print("Simulator closes.")
            return

        else:
            print("Erroneous command!")
            print("Enter one of these commands:")
            print("NR (new router)")
            print("P (print)")
            print("C (connect)")
            print("NN (new network)")
            print("PA (print all)")
            print("S (send routing tables)")
            print("RR (route request)")
            print("Q (quit)")


def add_routers_from_file(routers, routerfile):
    """ Handles the file to call the proper method
    :param routers: (dict) an empty database created in main function
    :param routerfile: (file) a file containing routers, neighbours, routes
    :return: (boolean) in case of error: False, in case of success: True
    """

    try:
        with open(routerfile, "r") as file:
            for line in file:
                line = line.strip().split("!")

                if line[0] != "":  # Line[0] means ROUTER NAME
                    add_new_router(routers, line[0], suppress_errors=True)

                    if line[1] != "":  # line[1] means LIST OF NEIGHBOURS
                        neighbours = line[1].split(";")
                        add_new_neighbours(routers, line[0], neighbours)

                    if line[2] != "":  # line[2] means LIST OF ROUTES
                        routes = line[2].split(";")
                        add_new_routes(routers, line[0], routes)

    except IOError:
        print("Error: the file could not be read "
              "or there is something wrong with it.")
        return False

    return True


def add_new_router(routers, router, suppress_errors=False):
    """ Creates and add new router object to the database
    :param routers: (dict) an empty database created in main function
    :param router: (str) name of the router
    :param suppress_errors: (boolean) if False: user command called the method
                                      if True: file processor called the method
    :return: None
    """

    if router not in routers:
        # Create new router object and adds to database
        routers[router] = Router(router)

    # It only passes when (user gives the command and router was already added)
    elif not suppress_errors:
        print("Name is taken.")


def add_new_neighbours(routers, router, neighbours):
    """ Calls proper method to add neighbour to the target router and vice versa
    :param routers: (dict) an empty database created in main function
    :param router: (str) name of the router
    :param neighbours: (list) list of neighbours
    :return: None
    """

    if router in routers:
        for neighbour in neighbours:
            if neighbour in routers:
                routers[router].add_neighbour(routers[neighbour])
                routers[neighbour].add_neighbour(routers[router])


def add_new_routes(routers, router, routes):
    """ Calls the proper method to add networks and distances
    :param routers: (dict) an empty database created in main function
    :param router: (str) name of the router
    :param routes: (list) list of routes
    :return: None
    """

    if router in routers:
        for route in routes:
            route = route.split(":")
            routers[router].add_network(str(route[0]), int(route[1]))


main()
